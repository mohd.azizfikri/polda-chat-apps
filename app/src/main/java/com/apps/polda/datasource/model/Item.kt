package com.apps.polda.datasource.model

import androidx.recyclerview.widget.DiffUtil
import com.apps.polda.utils.zeedata.api.response.itemresponse.ProvinceResponse

data class Item(
    val id: String,
    val name: String,

    //Addition
    var selected: Boolean = false
    ) {

    companion object {

        val DIFF_UTIL = object : DiffUtil.ItemCallback<Item>() {

            override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem.id == newItem.id
            }

        }

        fun mapFromProvince(province: ProvinceResponse): Item{
            return Item(
                province.province.orEmpty(),
                province.province.orEmpty()
            )
        }

    }

}