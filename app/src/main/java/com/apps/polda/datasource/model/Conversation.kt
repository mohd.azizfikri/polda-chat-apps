package com.apps.polda.datasource.model

import androidx.recyclerview.widget.DiffUtil

data class Conversation(
    val id: String,
    val name: String,
    val image: String,
    val message: String,
    val date: String,
    val isSender: Boolean,

    //Addition
    var selected: Boolean = false
) {

    companion object {

        val DIFF_UTIL = object : DiffUtil.ItemCallback<Item>() {

            override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem.id == newItem.id
            }

        }

    }

}