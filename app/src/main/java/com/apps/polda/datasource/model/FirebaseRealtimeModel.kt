package com.apps.polda.datasource.model

data class FirebaseRealtimeModel (
    var from: String = "",
    var status: Long = 0,
    var type: String = ""
)