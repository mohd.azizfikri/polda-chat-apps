package com.apps.polda.utils.zeedata.exception.error

class IncompleteSignInInformationError(message: String? = null) : Exception(message)