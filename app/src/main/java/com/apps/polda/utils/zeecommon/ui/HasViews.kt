package com.apps.polda.utils.zeecommon.ui

interface HasViews {

    fun setupViews()

}