package com.apps.polda.utils.zeedata.repository

import com.apps.polda.utils.convertImageToBase64
import com.apps.polda.utils.zeedata.api.response.general.MessageResponse
import com.apps.polda.utils.zeedata.api.response.itemresponse.ChatResponse
import com.apps.polda.utils.zeedata.api.response.itemresponse.ConversationResponse
import com.apps.polda.utils.zeedata.util.AppRepository
import com.natpryce.Result
import com.natpryce.Success
import com.natpryce.onFailure
import java.io.File

class ChatRepository: AppRepository() {

    suspend fun getListPrivateChat(filter: String? = null): Result<List<ChatResponse>, Exception> {
        val result = asResultList { getApi().getPrivateChatList(filter) }.onFailure { return it }

        return Success(result)
    }

    suspend fun getListConversation(username: String): Result<List<ConversationResponse>, Exception> {
        val result = asResultList { getApi().getPrivateChatCoverstion(username) }.onFailure { return it }

        return Success(result)
    }

    suspend fun postMessage(dest_username: String, chat: String, media_type: Int? = 0): Result<MessageResponse, Exception> {
        val cType = when(media_type){
            0 -> "text"
            1 -> "image"
            2 -> "video"
            else -> "location"
        }

        return asDirectResult { getApi().postMessage(dest_username, chat, cType) }
    }

    suspend fun getListGroupChat(): Result<List<ChatResponse>, Exception> {
        val result = asResultList { getApi().getGroupChatList() }.onFailure { return it }

        return Success(result)
    }

    suspend fun getListConversationGroup(username: String): Result<List<ConversationResponse>, Exception> {
        val result = asResultList { getApi().getGroupChatCoverstion(username) }.onFailure { return it }

        return Success(result)
    }

    suspend fun postMessageGroup(dest_username: String, chat: String, media_type: Int? = 0): Result<MessageResponse, Exception> {
        val cType = when(media_type){
            0 -> "text"
            1 -> "image"
            2 -> "video"
            else -> "location"
        }

        return asDirectResult { getApi().postMessageGroup(dest_username, chat, cType) }
    }

    suspend fun postImage(dest_username: String, content: File, isGroup: Boolean): Result<MessageResponse, Exception> {

        return when(isGroup){
            true -> asDirectResult {
                getApi().postImageGroup(dest_username, convertImageToBase64(content),  "image")
            }
            else -> asDirectResult {
                getApi().postImage(dest_username, convertImageToBase64(content),  "image")
            }
        }
    }

    suspend fun deleteChatFrom(username: String): Result<MessageResponse, Exception> {
        return asDirectResult { getApi().deleteChatFrom(username) }
    }

}