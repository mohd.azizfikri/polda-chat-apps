package com.apps.polda.utils.zeedata.repository

import com.apps.polda.utils.zeedata.api.response.itemresponse.ActionFriendReqResponse
import com.apps.polda.utils.zeedata.api.response.itemresponse.FriendResponse
import com.apps.polda.utils.zeedata.api.response.itemresponse.GroupInfoResponse
import com.apps.polda.utils.zeedata.util.AppRepository
import com.natpryce.Result
import com.natpryce.Success
import com.natpryce.onFailure

class ContactRepository: AppRepository() {

    suspend fun getFriendList(): Result<List<FriendResponse>, Exception> {
        val result = asResultList { getApi().getFriendList() }.onFailure { return it }

        return Success(result)
    }

    suspend fun searchUsername(usrname: String): Result<List<FriendResponse>, Exception> {
        val result = asResult { getApi().searchUsername(usrname) }.onFailure { return it }

        val list: ArrayList<FriendResponse> = ArrayList()
        list.add(result)
        return Success(list)
    }

    suspend fun sendReqAddFriend(idUser: String): Result<String, Exception> {
        val result = asResultMessage { getApi().sendFriendReq(idUser) }.onFailure { return it }

        return Success(result)
    }

    suspend fun getFriendReqList(): Result<List<FriendResponse>, Exception> {
        val result = asResultList { getApi().getFriendReqList() }.onFailure { return it }

        return Success(result)
    }

    suspend fun postActionFriendReq(idUser: Int, action: Int): Result<ActionFriendReqResponse, Exception> {
        val result = asResult { getApi().postActionFriendReq(idUser.toString(), action.toString()) }.onFailure { return it }

        return Success(result)
    }

    suspend fun getGroupInfo(id: String): Result<GroupInfoResponse, Exception> {
        val result = asResultList { getApi().getGroupInfo(id) }.onFailure { return it }

        return Success(result.first())
    }

}