package com.apps.polda.utils.zeecommon.ui

interface HasToolbarTitle {

    val toolbarTitle: String

}