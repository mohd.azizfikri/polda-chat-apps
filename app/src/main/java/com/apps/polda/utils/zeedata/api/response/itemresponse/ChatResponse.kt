package com.apps.polda.utils.zeedata.api.response.itemresponse

import com.google.gson.annotations.SerializedName

data class ChatResponse(
    @field:SerializedName("dest_username", alternate = ["id"])
    val destUsername: String? = null,

    @field:SerializedName("last_chat")
    val lastChat: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("total_unread")
    val totalUnread: Int? = null,

    @field:SerializedName("chat_session")
    val chatSession: String? = null,

    @field:SerializedName("profile_picture")
    val profilePicture: String? = null,

    @field:SerializedName("description")
    val description: String? = null
){
    @field:SerializedName("selected")
    var selected: Boolean = false
}
