package com.apps.polda.utils.zeecommon.ui

interface HasParentViewModel {

    val parentViewModel: AppViewModel

}