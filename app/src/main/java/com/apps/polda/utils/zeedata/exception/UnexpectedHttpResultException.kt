package com.apps.polda.utils.zeedata.exception

import com.apps.polda.utils.zeedata.api.response.general.MessageResponse

class UnexpectedHttpResultException(errorResponse: MessageResponse) :
    Exception(errorResponse.message ?: errorResponse.error)