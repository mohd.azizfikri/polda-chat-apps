package com.apps.polda.utils.zeedata.api.response.itemresponse

import androidx.room.Ignore
import com.google.gson.annotations.SerializedName

data class ConversationResponse(
    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("sender")
    val sender: Boolean = false,

    @field:SerializedName("username")
    val username: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("content")
    val content: String? = null,

    @field:SerializedName("file")
    val file: String? = null,

    @field:SerializedName("type")
    val type: Int = 1,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("read_at")
    val read_at: String? = null,

    @Ignore
    @field:SerializedName("is_group")
    var isGroup: Boolean = false
)