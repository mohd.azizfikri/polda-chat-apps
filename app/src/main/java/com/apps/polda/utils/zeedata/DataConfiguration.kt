package com.apps.polda.utils.zeedata

import com.apps.polda.utils.zeedata.db.AppDatabase
import com.apps.polda.utils.zeedata.prefs.AppPrefs

data class DataConfiguration(
    val remoteHost: String = "",
    val remoteTimeout: Long = 30,
    val databaseName: String = AppDatabase.DEFAULT_DATABASE_NAME,
    val prefsName: String = AppPrefs.DEFAULT_PREFS_NAME,
    val prefsAlwaysCommit: Boolean = true,
    val debug: Boolean = true
)