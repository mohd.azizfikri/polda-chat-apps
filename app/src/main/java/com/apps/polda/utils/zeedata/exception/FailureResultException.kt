package com.apps.polda.utils.zeedata.exception

class FailureResultException(message: String) : Exception(message)