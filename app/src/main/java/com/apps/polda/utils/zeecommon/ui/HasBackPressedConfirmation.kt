package com.apps.polda.utils.zeecommon.ui

interface HasBackPressedConfirmation {

    val backPressDelay: Long

}