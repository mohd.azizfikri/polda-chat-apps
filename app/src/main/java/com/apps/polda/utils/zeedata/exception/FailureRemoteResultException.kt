package com.apps.polda.utils.zeedata.exception

class FailureRemoteResultException(message: String) : Exception(message)