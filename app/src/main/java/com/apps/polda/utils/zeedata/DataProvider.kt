package com.apps.polda.utils.zeedata

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.apps.polda.utils.zeedata.api.ApiService
import com.apps.polda.utils.zeedata.api.interceptor.HeaderTokenInterceptor
import com.apps.polda.utils.zeedata.db.AppDatabase
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.*


object DataProvider {

    private lateinit var mApplicationContext: Application
    private lateinit var mDataConfiguration: DataConfiguration

    private lateinit var mApiService: ApiService
    private lateinit var mAppDatabase: AppDatabase

    private val mDefaultPreferences by lazy {
        getContext().getSharedPreferences(
            getConfig().prefsName,
            Context.MODE_PRIVATE
        )
    }

    @Synchronized
    fun init(app: Application, dataConfiguration: DataConfiguration = DataConfiguration()) {
        mApplicationContext = app
        mDataConfiguration = dataConfiguration

        mAppDatabase = provideAppDatabase(app, dataConfiguration)
        mApiService =
            provideRetrofit(dataConfiguration, mAppDatabase).create(ApiService::class.java)
    }

    fun getContext(): Context {
        return mApplicationContext
    }

    fun getConfig(): DataConfiguration {
        return mDataConfiguration
    }

    fun getPrefs(): SharedPreferences {
        return mDefaultPreferences
    }

    fun getApiService(): ApiService {
        return mApiService
    }

    fun getAppDatabase(): AppDatabase {
        return mAppDatabase
    }

    /**
     * Retrofit provider
     */
    private fun provideRetrofit(config: DataConfiguration, db: AppDatabase): Retrofit {
        return Retrofit
            .Builder()
            .baseUrl(config.remoteHost)
            .addConverterFactory(GsonConverterFactory.create())
            .client(provideRetrofitClient(config, db))
            .build()
    }

    private fun provideRetrofitClient(config: DataConfiguration, db: AppDatabase): OkHttpClient {
        // Create a trust manager that does not validate certificate chains
        val trustAllCerts = arrayOf<TrustManager>(
            object : X509TrustManager {
                @Throws(CertificateException::class)
                override fun checkClientTrusted(
                    chain: Array<X509Certificate?>?,
                    authType: String?
                ) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(
                    chain: Array<X509Certificate?>?,
                    authType: String?
                ) {
                }

                override fun getAcceptedIssuers(): Array<X509Certificate?>? {
                    return arrayOf()
                }
            }
        )

        // Install the all-trusting trust manager
        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, trustAllCerts, SecureRandom())

        // Create an ssl socket factory with our all-trusting manager
        val sslSocketFactory: SSLSocketFactory = sslContext.socketFactory

        val builder = OkHttpClient.Builder()
        builder.sslSocketFactory(
            sslSocketFactory,
            trustAllCerts[0] as X509TrustManager
        )
        builder.hostnameVerifier { _, _ -> true }

        return builder
            .addInterceptor(provideHeaderTokenInterceptor(db))
            .addInterceptor(provideHttpLoggingInterceptor(config))
            .writeTimeout(config.remoteTimeout, TimeUnit.SECONDS)
            .readTimeout(config.remoteTimeout, TimeUnit.SECONDS)
            .build()
    }

    private fun provideHttpLoggingInterceptor(config: DataConfiguration): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().apply {
            level = if (config.debug) HttpLoggingInterceptor.Level.BODY
            else HttpLoggingInterceptor.Level.NONE
        }
    }

    private fun provideHeaderTokenInterceptor(db: AppDatabase): HeaderTokenInterceptor {
        return HeaderTokenInterceptor(db)
    }

    /**
     * Room provider
     */
    private fun provideAppDatabase(context: Context, config: DataConfiguration): AppDatabase {
        return Room
            .databaseBuilder(context, AppDatabase::class.java, config.databaseName)
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()
    }

    /**
     * SSL Handshake
     */
    @SuppressLint("TrulyRandom")
    open fun handleSSLHandshake() {
        try {
            val trustAllCerts: Array<TrustManager> =
                arrayOf<TrustManager>(object : X509TrustManager {
                    override fun checkClientTrusted(certs: Array<X509Certificate?>?, authType: String?) {}
                    override fun checkServerTrusted(certs: Array<X509Certificate?>?, authType: String?) {}
                    override fun getAcceptedIssuers(): Array<X509Certificate> {
                        return emptyArray()
                    }
                })
            val sc: SSLContext = SSLContext.getInstance("SSL")
            sc.init(null, trustAllCerts, SecureRandom())
            HttpsURLConnection
                .setDefaultSSLSocketFactory(sc.socketFactory)
            HttpsURLConnection.setDefaultHostnameVerifier { _, _ -> true }
        } catch (ignored: Exception) {
        }
    }


}