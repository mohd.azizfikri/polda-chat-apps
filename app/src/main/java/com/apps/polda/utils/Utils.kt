package com.apps.polda.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.widget.DatePicker
import android.widget.Toast
import com.apps.polda.datasource.model.Item
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

fun sampleList(): ArrayList<String>{
    val sample: ArrayList<String> = ArrayList()
    sample.add("1")
    sample.add("2")
    sample.add("3")
    sample.add("4")
    sample.add("5")
    sample.add("6")
    return sample
}

fun sampleList(num: Int): ArrayList<String>{
    val sample: ArrayList<String> = ArrayList()
    for (i in 1..num) {
        sample.add(i.toString())
    }
    return sample
}

fun listMetodeBayar(): ArrayList<Item>{
    val sample: ArrayList<Item> = ArrayList()
    sample.add(Item("kredit","KPR"))
    sample.add(Item("cash","Cash"))
    sample.add(Item("online","Online Payment"))
    return sample
}


fun listJangkaWaktu(tahun: Boolean = true): ArrayList<Item>{
    val sample: ArrayList<Item> = ArrayList()
    when {
        tahun -> {
            sample.add(Item("1","1 Tahun"))
            sample.add(Item("2","2 Tahun"))
            sample.add(Item("3","3 Tahun"))
            sample.add(Item("4","4 Tahun"))
            sample.add(Item("5","5 Tahun"))
            sample.add(Item("6","6 Tahun"))
            sample.add(Item("7","7 Tahun"))
            sample.add(Item("8","8 Tahun"))
            sample.add(Item("9","9 Tahun"))
        }
        else -> {
            sample.add(Item("1","1 Bulan"))
            sample.add(Item("2","2 Bulan"))
            sample.add(Item("3","3 Bulan"))
            sample.add(Item("4","4 Bulan"))
            sample.add(Item("5","5 Bulan"))
            sample.add(Item("6","6 Bulan"))
            sample.add(Item("7","7 Bulan"))
            sample.add(Item("8","8 Bulan"))
            sample.add(Item("9","9 Bulan"))
            sample.add(Item("10","10 Bulan"))
            sample.add(Item("11","11 Bulan"))
            sample.add(Item("12","1 Tahun"))
            sample.add(Item("24","2 Tahun"))
            sample.add(Item("36","3 Tahun"))
            sample.add(Item("48","4 Tahun"))
            sample.add(Item("60","5 Tahun"))
            sample.add(Item("72","6 Tahun"))
            sample.add(Item("84","7 Tahun"))
        }
    }
    return sample
}

fun Context.toast(text: String) {
    Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
}

fun setDateFormat(date: String, formatAwal: String): String {
    val format = SimpleDateFormat(formatAwal)
    val finals = SimpleDateFormat("dd MMM yyyy")
    val spDate = format.parse(date)

    return finals.format(spDate)
}

fun getCurrentDate(): String {
    val calendar = Calendar.getInstance()
    val mdformat = SimpleDateFormat("dd-MM-yyyy")

    return mdformat.format(calendar.time)
}

fun getDateFromDatePicker(datePicker: DatePicker): String {
    val day = datePicker.dayOfMonth
    val month = datePicker.month
    val year = datePicker.year

    val calendar = Calendar.getInstance()
    calendar.set(year, month, day)

    val format = SimpleDateFormat("dd-MM-yyyy")

    return format.format(calendar.time)
}

fun stringToDate(date: String, format: String): Date {
    val formater = SimpleDateFormat(format)

    return formater.parse(date)
}

fun changeDateFormat(date: String, formatAwal: String, formatAkhir: String): String {
    val format = SimpleDateFormat(formatAwal)
    val finals = SimpleDateFormat(formatAkhir)
    val spDate = format.parse(date)

    return finals.format(spDate)
}

fun convertImageToBase64(image: File): String {
    val bm = BitmapFactory.decodeFile(image.path)
    val baos = ByteArrayOutputStream()
    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos) // bm is the bitmap object

    val b: ByteArray = baos.toByteArray()
    val type = "data:image/" + image.extension + ";base64,"

    return type + Base64.encodeToString(b, Base64.DEFAULT)
}
