package com.apps.polda.utils.zeedata.api.response.itemresponse

data class FriendResponse(
	val name: String? = null,
	val created_at: String? = null,
	val id: Int? = null,
	val profile_picture: String? = null,
	val email: String? = null,
	val username: String? = null,
	val is_friend: Boolean = false
)

