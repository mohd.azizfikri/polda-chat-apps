package com.apps.polda.utils.zeewidget

import androidx.appcompat.app.AppCompatActivity
import android.annotation.SuppressLint
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.widget.Toast
import androidx.core.view.setPadding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.apps.polda.R
import com.apps.polda.databinding.ZeemapsLocationPickerBinding
import java.io.IOException
import java.util.*

class ZeePlacePicker : AppCompatActivity() {
    lateinit var binding: ZeemapsLocationPickerBinding

    var lat: Double = -6.9034443
    var lng: Double = 107.5731162

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ZeemapsLocationPickerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        lat = intent.getDoubleExtra("LAT",-6.9034443)
        lng = intent.getDoubleExtra("LNG",107.5731162)

        binding.textTitle.text = getAddress(LatLng(lat,lng))
        binding.textAddress.text = "$lat, $lng"

        val maps = supportFragmentManager.findFragmentById(R.id.map_view) as SupportMapFragment
        maps.getMapAsync { map ->
            val placeNow = LatLng(lat, lng)
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(placeNow, 18f))

            val oldPosition = map.cameraPosition.target

            map.setOnCameraMoveStartedListener {
                // drag started

                // start animation
                binding.iconMarker.animate().translationY(-50f).start()
                binding.iconMarkerShadow.animate().withStartAction {
                    binding.iconMarkerShadow.setPadding(10)
                }.start()
            }

            map.setOnCameraIdleListener {
                val newPosition = map.cameraPosition.target
                if (newPosition != oldPosition) {
                    // drag ended
                    lat = newPosition.latitude
                    lng = newPosition.longitude

                    // start animation
                    binding.iconMarker.animate().translationY(0f).start()
                    binding.iconMarkerShadow.animate().withStartAction {
                        binding.iconMarkerShadow.setPadding(0)
                    }.start()

                    binding.textTitle.text = getAddress(newPosition)
                    binding.textAddress.text = lat.toString() +", "+ lng.toString()

                }
            }
        }

        binding.btnCancel.setOnClickListener {
            val returnIntent = Intent()
            setResult(RESULT_CANCELED, returnIntent)
            finish()
        }

        binding.btnOkay.setOnClickListener {
            val returnIntent = Intent()
            returnIntent.putExtra("LAT", lat)
            returnIntent.putExtra("LNG", lng)
            setResult(RESULT_OK, returnIntent)
            finish()
        }
    }

    fun getAddress(latLng: LatLng): String {
        val geocoder = Geocoder(applicationContext, Locale.getDefault())
        try {
            val addresses: List<Address> = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
            val obj: Address = addresses[0]
            var add: String = obj.getAddressLine(0)
            add = """$add ${obj.getCountryName()}""".trimIndent()

            return add
        } catch (e: IOException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
            Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
            return e.localizedMessage
        }
    }
}