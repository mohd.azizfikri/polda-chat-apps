package com.apps.polda.utils.zeedata.repository

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import com.apps.polda.utils.zeedata.api.request.UpdateProfileBody
import com.apps.polda.utils.zeedata.api.response.itemresponse.ProfileResponse
import com.apps.polda.utils.zeedata.db.entity.Configuration
import com.apps.polda.utils.zeedata.util.AppRepository
import com.natpryce.*
import com.apps.polda.utils.*
import java.io.ByteArrayOutputStream
import java.io.File

class ProfileRepository : AppRepository() {

    suspend fun signIn(user: String, pass: String): Result<String, Exception> {
        val result = asResult { getApi().login(user, pass) }.onFailure { return it }

        getAppDb().configurationDao().clear()

        result.let { data ->
            // save token immediately
            val config1 = Configuration(Configuration.KEY_TOKEN, data.token)
            val config2 = Configuration(Configuration.KEY_USERNAME, user)

            getAppDb()
                .configurationDao()
                .insert(config1, config2)

        }

        return Success(result.token)
    }

    fun tokenFromDB(): Result<String, Exception> {
        val token = getAppDb()
            .configurationDao()
            .getOneFromMainThread(Configuration.KEY_TOKEN)
            ?.content

        return if (token != null) Success(token)
        else Failure(Exception("No Token"))
    }

    fun usernameFromDB(): Result<String, Exception> {
        val username = getAppDb()
            .configurationDao()
            .getOneFromMainThread(Configuration.KEY_USERNAME)
            ?.content

        return if (username != null) Success(username)
        else Failure(Exception("No Username"))
    }

    suspend fun register(email: String, phone: String, fulname: String, pass: String): Result<String, Exception> {
        val result = asResult { getApi().register(email, phone, fulname, pass, pass)  }.onFailure { return it }

        return when {
            result.email != null -> Failure(Exception(result.email[0]))
            result.username != null -> Failure(Exception(result.username[0]))
            result.name != null -> Failure(Exception(result.name[0]))
            result.password != null -> Failure(Exception(result.password[0]))
            result.password_confirmation != null -> Failure(Exception(result.password_confirmation[0]))
            else -> Success(result.nextUrl)
        }
    }

    suspend fun otpvalidation(code: String): Result<Boolean, Exception> {
        val result = asResult { getApi().verify(code) }.onFailure { return it }
//        return Success(true)

        return if(code.contains("8",true)){
            Success(true)
        }else{
            Failure(Exception("Gagal aja"))
        }
    }

    suspend fun validateToken() =
        asDirectResult { getApi().validateToken() }

    suspend fun updateFCM(token: String): Result<String, Exception> {
        asDirectResult { getApi().update_fcm(token) }.onFailure { return it }
        return Success("Success")
    }

    suspend fun registProfile(province: String, image:  File? = null) {

        if(image != null) {
         /*   val bm = BitmapFactory.decodeFile(image.path)
            val baos = ByteArrayOutputStream()
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos) // bm is the bitmap object

            val b: ByteArray = baos.toByteArray()
            val type = "data:image/"+ image.extension + ";base64,"
            val encodedImage: String = type + Base64.encodeToString(b, Base64.DEFAULT)

            asDirectResult { getApi().eTest(encodedImage) }*/

            asDirectResult { getApi().regist_profile(
                province, convertImageToBase64(image)
            ) }
        }else {
            asDirectResult { getApi().regist_profile(
                province
            ) }
        }

    }

    suspend fun getProfile(): Result<ProfileResponse, Exception> {
        val result = asDirectResult { getApi().getProfile() }.onFailure { return it }
        return Success(result.data!!)
    }

    suspend fun updateProfile(name: String, image:  File? = null) {
        val body = UpdateProfileBody()
        body.name = name

        if(image != null) {
           /* val bm = BitmapFactory.decodeFile(image.path)
            val baos = ByteArrayOutputStream()
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos) // bm is the bitmap object

            val b: ByteArray = baos.toByteArray()
            val encodedImage: String = Base64.encodeToString(b, Base64.DEFAULT)*/

            body.profile_picture = convertImageToBase64(image)
        }

        asDirectResult { getApi().updateUser(body) }
    }

    suspend fun logout() =
        asDirectResult { getApi().logout() }

}