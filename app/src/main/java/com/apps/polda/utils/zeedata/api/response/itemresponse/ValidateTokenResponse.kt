package com.apps.polda.utils.zeedata.api.response.itemresponse

data class ValidateTokenResponse(
	val is_has_province: Boolean? = null,
	val id: Int? = null,
	val username: String? = null,
	val name: String? = null,
	val validateToken: String? = null
)

