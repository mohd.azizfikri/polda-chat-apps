package com.apps.polda.utils.zeedata.repository

import com.natpryce.Result
import com.natpryce.Success
import com.natpryce.onFailure
import com.apps.polda.utils.zeedata.api.response.itemresponse.*
import com.apps.polda.utils.zeedata.util.AppRepository

class CommonRepository : AppRepository()  {

   /* suspend fun getAboutUs(): Result<TermsAppResponse, Exception> {
        val result = asResultList { getApi().getAboutUs() }.onFailure { return it }
        return Success(result.first())
    }

    suspend fun getQNA(): Result<List<QnaResponse>, Exception> {
        val result = asResultList { getApi().getQNA() }.onFailure { return it }
        return Success(result)
    }

    suspend fun getQNA(search: String): Result<List<QnaResponse>, Exception> {
        val result = asResultList { getApi().getQNA(search) }.onFailure { return it }
        return Success(result)
    }

    suspend fun getTermsApp(): Result<List<TermsAppResponse>, Exception> {
        val result = asResultList { getApi().getTermsApp() }.onFailure { return it }
        return Success(result)
    }

    suspend fun getArea(idProvince: String): Result<List<AreaResponse>, Exception> {
        val result = asResultList { getApi().getArea(idProvince) }.onFailure { return it }
        return Success(result)
    }
*/
    suspend fun getProvince(): Result<List<ProvinceResponse>, Exception> {
        val result = asResultList { getApi().getProvince() }.onFailure { return it }
        return Success(result)
    }

    suspend fun getListNotif(): Result<List<String>, Exception> {
        return Success(emptyList())
    }

    suspend fun getVideo(filter: String? = null): Result<List<VideoResponse>, Exception> {
        val result = asResultList { getApi().getVideo(filter) }.onFailure { return it }
        return Success(result)
    }

    suspend fun searchVideo(filter: String? = null): Result<List<VideoResponse>, Exception> {
        val result = asResultList { getApi().searchVideo(filter) }.onFailure { return it }
        return Success(result)
    }

    suspend fun getPanduan(): Result<List<PanduanResponse>, Exception> {
        val result = asResultList { getApi().getPanduan() }.onFailure { return it }
        return Success(result)
    }

    suspend fun getPanduan(keyword: String): Result<List<PanduanResponse>, Exception> {
        val result = asResultList { getApi().getPanduan(keyword) }.onFailure { return it }
        return Success(result)
    }



}