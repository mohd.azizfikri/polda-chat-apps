package com.apps.polda.utils.zeedata.api.response.itemresponse

import com.google.gson.annotations.SerializedName

data class VideoResponse(
	@field:SerializedName("author")
	val author: String? = null,

	@field:SerializedName("link")
	val link: String? = null,

	@field:SerializedName("thumbnail")
	val thumbnail: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("created_at", alternate = ["createdAt"])
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("category")
	val category: String? = null
)

