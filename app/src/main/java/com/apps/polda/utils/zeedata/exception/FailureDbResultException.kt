package com.apps.polda.utils.zeedata.exception

class FailureDbResultException(message: String) : Exception(message)