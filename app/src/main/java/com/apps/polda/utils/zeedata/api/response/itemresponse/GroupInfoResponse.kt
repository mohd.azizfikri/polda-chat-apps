package com.apps.polda.utils.zeedata.api.response.itemresponse

import com.apps.polda.utils.zeedata.api.response.itemresponse.utils.MemberInfo
import com.google.gson.annotations.SerializedName

data class GroupInfoResponse(

    @SerializedName("description")
    var mDescription: String? = null,

    @SerializedName("id")
    val mId: Long? = null,

    @SerializedName("member_info")
    val mMemberInfo: List<MemberInfo>? = null,

    @SerializedName("name")
    val mName: String? = null,

    @SerializedName("profile_picture")
    val mProfilePicture: String? = null,

    @SerializedName("total_member")
    val mTotalMember: Int? = null
)
