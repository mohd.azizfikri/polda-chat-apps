package com.apps.polda.utils.zeedata.api.request

import com.google.gson.annotations.SerializedName

class UpdateProfileBody (
    @SerializedName("name")
    var name: String? = "",

    @SerializedName("profile_picture")
    var profile_picture: String? = ""
)