package com.apps.polda.utils.zeedata.api.response.itemresponse

data class ProvinceResponse(
	val province: String? = null
)

