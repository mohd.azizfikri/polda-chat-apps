package com.apps.polda.utils.zeedata.api.response.itemresponse

data class ProfileResponse(
	val name: String? = null,
	val id: Int? = null,
	val email: String? = null,
	val username: String? = null,

	val province: String? = null,
	val profile_picture: String? = null,
	val birthday: String? = null,
	val about: String? = null,
	val religion: String? = null,
	val occupation: String? = null

)

