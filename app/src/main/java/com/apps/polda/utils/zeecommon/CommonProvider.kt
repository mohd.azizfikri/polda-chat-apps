package com.apps.polda.utils.zeecommon

object CommonProvider {

    private var mViewModelBindingId: Int? = null

    fun init(viewModelBindingId: Int) {
        mViewModelBindingId = viewModelBindingId
    }

    fun getViewModelBindingId() = mViewModelBindingId

}