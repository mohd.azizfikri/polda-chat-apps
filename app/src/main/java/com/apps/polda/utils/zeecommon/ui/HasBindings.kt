package com.apps.polda.utils.zeecommon.ui

interface HasBindings {

    fun setupBinding()

}