package com.apps.polda.utils.zeedata.exception

class UnexpectedRepositoryResultException(message: String = "Unexpected error occured."): Exception(message)