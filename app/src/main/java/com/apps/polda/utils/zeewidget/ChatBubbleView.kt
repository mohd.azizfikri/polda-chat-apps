package com.apps.polda.utils.zeewidget

import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import com.apps.polda.R
import java.util.*


class ChatBubbleView : FrameLayout {

    private var mMessage: String? = null
    private var mTime: String? = null
    private var mLocation: String? = null
    private var mSender: String? = null

    private var ivImage: ImageView? = null
    private var layoutMap: RelativeLayout? = null
    private var tvMessage: TextView? = null
    private var tvTime: TextView? = null
    private var tvSender: TextView? = null

    constructor(@NonNull context: Context) : super(context)

    constructor(@NonNull context: Context, @Nullable attrs: AttributeSet) : super(context, attrs) {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.ChatBubbleView, 0, 0)

        setType(typedArray.getInt(R.styleable.ChatBubbleView_chat_type, TYPE_OUT))

        mMessage = typedArray.getString(R.styleable.ChatBubbleView_chat_message)
        mTime = typedArray.getString(R.styleable.ChatBubbleView_chat_time)
        mSender = typedArray.getString(R.styleable.ChatBubbleView_chat_sender)
        mLocation = typedArray.getString(R.styleable.ChatBubbleView_chat_location)

        setMessage(mMessage)
        setTime(mTime)

        typedArray.recycle()

        initViews()
    }

    fun initViews() {
        ivImage = findViewById(R.id.iv_chat_image)
        tvMessage = findViewById(R.id.tv_chat_message)
        tvTime = findViewById(R.id.tv_chat_time)
        tvSender = findViewById(R.id.tv_sender)
        layoutMap = findViewById(R.id.layout_location)
    }

    fun setType(type: Int) {
        val resLayout = if (type == TYPE_IN) R.layout.view_chat_bubble_in else R.layout.view_chat_bubble_out

        removeAllViews()
        LayoutInflater.from(context).inflate(resLayout, this, true)

        initViews()
    }

    fun setMessage(text: String?) {
        mMessage = text
        tvMessage?.text = text
    }

    fun setTime(text: String?) {
        mTime = text
        tvTime?.text = text
    }

    fun setSender(loadSenderName: (view: TextView?) -> Unit) {
        loadSenderName(tvSender)
    }

    fun setImage(loadImage: (view: ImageView?) -> Unit) {
        loadImage(ivImage)
    }

    fun setImageResource(resId: Int) {
        ivImage?.setImageResource(resId)
    }

    fun setLocation(showLocation: Boolean, latLng: String? = ""){
        if(showLocation) {
            layoutMap?.visibility = View.VISIBLE
            val lat = latLng!!.split(",")[0].toDouble()
            val lng = latLng.split(",")[1].toDouble()

            val geocoder = Geocoder(context, Locale.getDefault())
            val addresses: List<Address> = geocoder.getFromLocation(lat, lng, 1)
            val obj: Address = addresses[0]
            var add: String = obj.getAddressLine(0)
            add = """$add ${obj.countryName}""".trimIndent()

            tvMessage?.text = add

            val uri = java.lang.String.format(Locale.ENGLISH, "geo:%f,%f", lat, lng)
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            layoutMap?.setOnClickListener { context.startActivity(intent) }
            tvMessage?.setOnClickListener { context.startActivity(intent) }

        }else{
            layoutMap?.visibility = View.GONE
        }
    }

    companion object {
        const val TYPE_IN = 0
        const val TYPE_OUT = 1
    }

}