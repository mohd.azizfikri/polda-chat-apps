package com.apps.polda.utils.zeecommon

data class SelectableData<T>(
    var selected: Boolean = false,
    val origin: T
)