package com.apps.polda.utils.zeedata.exception

class IllegalRemoteResultException(message: String) : Exception(message)