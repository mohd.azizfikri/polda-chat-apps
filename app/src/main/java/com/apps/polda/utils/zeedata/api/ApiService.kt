package com.apps.polda.utils.zeedata.api

import com.apps.polda.utils.zeedata.api.request.UpdateProfileBody
import com.apps.polda.utils.zeedata.api.response.general.DataListResponse
import com.apps.polda.utils.zeedata.api.response.general.DataResponse
import com.apps.polda.utils.zeedata.api.response.general.MessageResponse
import com.apps.polda.utils.zeedata.api.response.itemresponse.*
import retrofit2.Response
import retrofit2.http.*


interface ApiService {

    /**
     * USER ===============================================================================
     */

    @FormUrlEncoded
    @POST("polda/index.php/api/user/generateToken")
    suspend fun login(
        @Field("username") username: String,
        @Field("password") password: String
    ): Response<DataResponse<LoginResponse>>

    @FormUrlEncoded
    @POST("polda/index.php/api/user/register")
    suspend fun register(
        @Field("email") email: String,
        @Field("username") username: String,
        @Field("name") name: String,
        @Field("password") password: String,
        @Field("password_confirmation") password_confirmation: String
    ): Response<DataResponse<RegisterResponse>>

    @FormUrlEncoded
    @POST("polda/index.php/api/user/verify")
    suspend fun verify(
        @Field("code") code: String
    ): Response<DataResponse<LoginResponse>>

    @POST("polda/index.php/api/user/validateToken")
    suspend fun validateToken(): Response<DataResponse<ValidateTokenResponse>>

    @FormUrlEncoded
    @POST("polda/index.php/api/user/updatefcm")
    suspend fun update_fcm(
        @Field("fcm_id") fcm_id: String
    ): Response<MessageResponse>

    @FormUrlEncoded
    @POST("polda/index.php/api/user/profile/create")
    suspend fun regist_profile(
        @Field("province") province: String,
    ): Response<DataResponse<ProfileResponse>>

    @FormUrlEncoded
    @POST("polda/index.php/api/user/profile/create")
    suspend fun regist_profile(
        @Field("province") province: String,
        @Field("profile_picture") profile_picture: String
    ): Response<DataResponse<ProfileResponse>>

    @GET("polda/index.php/api/user/profile")
    suspend fun getProfile(): Response<DataResponse<ProfileResponse>>

    @POST("polda/index.php/api/user/logout")
    suspend fun logout(): Response<MessageResponse>

    @PUT("polda/index.php/api/user/profile/edit")
    suspend fun updateUser(
        @Body body: UpdateProfileBody?
    ): Response<DataResponse<ProfileResponse>>

    /**
     * FEATURE ===============================================================================
     */

    @GET("polda/index.php/api/video/")
    suspend fun getVideo(
        @Query("filter") filter: String? = null
    ): Response<DataListResponse<VideoResponse>>

    @GET("polda/index.php/api/video/")
    suspend fun searchVideo(
        @Query("title") title: String? = null
    ): Response<DataListResponse<VideoResponse>>

    @GET("polda/index.php/api/guidebook")
    suspend fun getPanduan(): Response<DataListResponse<PanduanResponse>>

    @GET("polda/index.php/api/guidebook")
    suspend fun getPanduan(
        @Query("title") title: String
    ): Response<DataListResponse<PanduanResponse>>

    /**
     * CHAT ===============================================================================
     */

    @GET("polda/index.php/api/chat/list")
    suspend fun getPrivateChatList(
        @Query("filter") filter: String? = null
    ): Response<DataListResponse<ChatResponse>>

    @GET("polda/index.php/api/chat/conversation/{username}")
    suspend fun getPrivateChatCoverstion(
        @Path("username") username: String
    ): Response<DataListResponse<ConversationResponse>>

    @FormUrlEncoded
    @POST("polda/index.php/api/chat/push")
    suspend fun postMessage(
        @Field("dest_username") destination: String,
        @Field("chat") chat: String,
        @Field("media_type") media_type: String,
    ): Response<MessageResponse>

    @FormUrlEncoded
    @POST("polda/index.php/api/chat/push")
    suspend fun postImage(
        @Field("dest_username") destination: String,
        @Field("media_file") media_file: String,
        @Field("media_type") media_type: String,
    ): Response<MessageResponse>

    @GET("polda/index.php/api/group/list")
    suspend fun getGroupChatList(): Response<DataListResponse<ChatResponse>>

    @GET("polda/index.php/api/group/chat/conversation/{username}")
    suspend fun getGroupChatCoverstion(
        @Path("username") username: String
    ): Response<DataListResponse<ConversationResponse>>

    @FormUrlEncoded
    @POST("polda/index.php/api/group/chat/push")
    suspend fun postMessageGroup(
        @Field("group_id") destination: String,
        @Field("chat") chat: String,
        @Field("media_type") media_type: String,
    ): Response<MessageResponse>

    @FormUrlEncoded
    @POST("polda/index.php/api/group/chat/push")
    suspend fun postImageGroup(
        @Field("group_id") destination: String,
        @Field("media_file") media_file: String,
        @Field("media_type") media_type: String,
    ): Response<MessageResponse>

    @POST("polda/index.php/api/chat/conversation/delete/{dest_username}")
    suspend fun deleteChatFrom(
        @Path("dest_username") destination: String
    ): Response<MessageResponse>

    /**
     * CONTACT ===============================================================================
     */

    @GET("polda/index.php/api/friend/list")
    suspend fun getFriendList(): Response<DataListResponse<FriendResponse>>

    @GET("polda/index.php/api/friend/search")
    suspend fun searchUsername(
        @Query("queryData") queryData: String
    ): Response<DataResponse<FriendResponse>>

    @POST("polda/index.php/api/friend/request/{id_user}")
    suspend fun sendFriendReq(
        @Path("id_user") id_user: String
    ): Response<MessageResponse>

    @GET("polda/index.php/api/friend/request/list")
    suspend fun getFriendReqList(): Response<DataListResponse<FriendResponse>>

    @FormUrlEncoded
    @POST("polda/index.php/api/friend/request/action/{id_user}")
    suspend fun postActionFriendReq(
        @Path("id_user") id_user: String,
        @Field("action") action: String
    ): Response<DataResponse<ActionFriendReqResponse>>


    @GET("polda/index.php/api/group/info/{id_group}")
    suspend fun getGroupInfo(
        @Path("id_group") id_group: String
    ): Response<DataListResponse<GroupInfoResponse>>

    /**
     * COMMON ===============================================================================
     */

    @GET("polda/index.php/api/province/list")
    suspend fun getProvince(
    ): Response<DataListResponse<ProvinceResponse>>


/*
    /**
     * USER ===============================================================================
     */

    @FormUrlEncoded
    @POST("polda/index.php/api/user/generateToken")
    suspend fun login(
        @Field("username") username: String,
        @Field("password") password: String
    ): Response<DataResponse<LoginResponse>>

    @FormUrlEncoded
    @POST("api/v1/auth/register")
    suspend fun register(
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("phone") phone: String,
        @Field("password") password: String,
        @Field("role") role: String
    ): Response<CobaRegist>

    @FormUrlEncoded
    @POST("api/v1/sendEmailReset")
    suspend fun sentEmailResetPassword(
        @Field("email") email: String
    ): Response<ErrorSentEmailResetPasswordResponse>

    /**
     * PROFILE ===============================================================================
     */

    @GET("api/v1/auth/profile")
    suspend fun getProfile(): Response<DataResponse<ProfileResponse>>

    @Multipart
    @POST("api/v1/auth/editProfile")
    suspend fun editProfile(
        @Part name: MultipartBody.Part,
        @Part email: MultipartBody.Part,
        @Part telepon: MultipartBody.Part,
        @Part image: MultipartBody.Part
    ): Response<MessageResponse>

    @FormUrlEncoded
    @POST("api/v1/reset_password")
    suspend fun resetPassword(
        @Field("old_password") old_password: String,
        @Field("password") password: String,
        @Field("temp_pass") temp_pass: String
    ): Response<MessageResponse>

    @GET("api/v1/tentang-kami")
    suspend fun getAboutUs(): Response<DataListResponse<TermsAppResponse>>

    @GET("api/v1/tanya-jawab")
    suspend fun getQNA(
        @Query("key") key: String = ""
    ): Response<DataListResponse<QnaResponse>>

    @GET("api/v1/syarat-ketentuan-aplikasi")
    suspend fun getTermsApp(): Response<DataListResponse<TermsAppResponse>>


    /**
     * ARTICLE ===============================================================================
     */

    @GET("api/v1/artikel/{id_category}/detail")
    suspend fun getArticle(
        @Path("id_category") id_category: String
    ): Response<DataResponse<ArticleResponse>>

    @GET("api/v1/artikel/")
    suspend fun getArticleLimit(
        @Query("limit") limit: String = "8"
    ): Response<CobaArtikel>


    /**
     * CHAT ===============================================================================
     */

    @GET("api/v1/chat/list")
    suspend fun getListChat(): Response<DataResponse<HandlerChatList>>

    @FormUrlEncoded
    @POST("api/v1/chat")
    suspend fun getConversation(
        @Field("project_id") projectId: String,
    ): Response<DataResponse<HandlerChatList>>

    @FormUrlEncoded
    @POST("api/v1/chat/kirim")
    suspend fun postConversation(
        @Field("project_id") projectId: String,
        @Field("chat") chat: String,
    ): Response<DataResponse<HandlerChatList>>

    @Multipart
    @FormUrlEncoded
    @POST("api/v1/chat/kirim")
    suspend fun postConversation(
        @Part projectId: MultipartBody.Part,
        @Part file: MultipartBody.Part,
    ): Response<DataResponse<HandlerChatList>>

    @Multipart
    @FormUrlEncoded
    @POST("api/v1/chat/kirim")
    suspend fun postConversation(
        @Part projectId: MultipartBody.Part,
        @Part chat: MultipartBody.Part,
        @Part file: MultipartBody.Part,
    ): Response<DataResponse<HandlerChatList>>


    /**
     * KPR ===============================================================================
     */

    @FormUrlEncoded
    @POST("api/v1/kpr/simulasi")
    suspend fun hitungKPR(
        @Field("harga") harga: String,
        @Field("dp") dp: String,
        @Field("bunga") bunga: String,
        @Field("jangka_waktu") jangka_waktu: String
    ): Response<DataResponse<HitungKPRResponse>>

    @POST("api/v1/kpr/pengajuan")
    suspend fun pengajuanKPR(
        @Body body: PengajuanKPRRequest
    ): Response<DataResponse<PengajuanKPRResponse>>

    @GET("api/v1/Prov")
    suspend fun getProvince(
    ): Response<DataListResponse<ProvinceResponse>>

    @GET("api/v1/Area")
    suspend fun getArea(
        @Query("prov_id") prov_id: String
    ): Response<DataListResponse<AreaResponse>>

    /**
     * ORDER ===============================================================================
     */

    @GET("api/v1/order/type_order")
    suspend fun getOrderType(
    ): Response<DataListResponse<OrderTypeResponse>>

    @GET("api/v1/order/tipe_rumah")
    suspend fun getTypeRumah(
    ): Response<DataListResponse<TypeRumahResponse>>


    @GET("api/v1/projek/spek")
    suspend fun getSpesification(
    ): Response<DataListResponse<SpekResponse>>

    @Multipart
    @POST("api/v1/order/desain")
    suspend fun postOrder(
        @Part a1: MultipartBody.Part,
        @Part a2: MultipartBody.Part,
        @Part a3: MultipartBody.Part,
        @Part a4: MultipartBody.Part,
        @Part a5: MultipartBody.Part,
        @Part a6: MultipartBody.Part,
        @Part a7: MultipartBody.Part,
        @Part a8: MultipartBody.Part,
        @Part a9: MultipartBody.Part,
        @Part a10: MultipartBody.Part,
        @Part a11: MultipartBody.Part,
        @Part a12: MultipartBody.Part,
        @Part a13: MultipartBody.Part,
        @Part a14: MultipartBody.Part,
        @Part a15: MultipartBody.Part,
        @Part a16: MultipartBody.Part,
        @Part a17: MultipartBody.Part,
        @Part a18: MultipartBody.Part,
        @Part a19: MultipartBody.Part,
        @Part denah: MultipartBody.Part,
        @Part rumah: MultipartBody.Part
    ): Response<MessageResponse>

    @Multipart
    @POST("api/v1/order")
    suspend fun postOrder(
        @Part a1: MultipartBody.Part,
        @Part a2: MultipartBody.Part,
        @Part a3: MultipartBody.Part,
        @Part a4: MultipartBody.Part,
        @Part a5: MultipartBody.Part,
        @Part a6: MultipartBody.Part,
        @Part a7: MultipartBody.Part,
        @Part a8: MultipartBody.Part,
        @Part a9: MultipartBody.Part,
        @Part a10: MultipartBody.Part,
        @Part a11: MultipartBody.Part,
        @Part a12: MultipartBody.Part,
        @Part a13: MultipartBody.Part,
        @Part a14: MultipartBody.Part,
        @Part rumah: MultipartBody.Part
    ): Response<MessageResponse>

    /**
     * PROJECT ===============================================================================
     */

    @GET("api/v1/promo/{id_category}/detail")
    suspend fun getPromo(
        @Path("id_category") id_category: String
    ): Response<DataResponse<HomePromoResponse>>

    @GET("api/v1/projek")
    suspend fun getAllProject(
        @Query("limit") limit: String = ""
    ): Response<DataListResponse<ProjectResponse>>

    @GET("api/v1/projek")
    suspend fun getProjectProgress(
        @Query("status") status: String,
        @Query("limit") limit: String = ""
    ): Response<DataListResponse<ProjectResponse>>

    @GET("api/v1/projek")
    suspend fun getProjectDone(
        @Query("status") status: String,
        @Query("limit") limit: String = ""
    ): Response<DataListResponse<ProjectResponse>>

    @GET("api/v1/projek/spek")
    suspend fun getProjectSpek(): Response<DataListResponse<ProjectResponse>>

    @GET("api/v1/projek/{id_project}/detail")
    suspend fun getDetailProject(
        @Path("id_project") id_project: String
    ): Response<CustomDetailResponse>
*/
}