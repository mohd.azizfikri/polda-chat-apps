package com.apps.polda.utils.zeedata.api.response.itemresponse

data class ActionFriendReqResponse(
	val name: String? = null,
	val action: String? = null,
	val id: Int? = null,
	val email: String? = null,
	val username: String? = null
)

