package com.apps.polda.utils.zeedata.api.interceptor

import com.apps.polda.utils.zeedata.db.AppDatabase
import com.apps.polda.utils.zeedata.db.entity.Configuration
import okhttp3.Request

class HeaderTokenInterceptor(private val mDb: AppDatabase) : AdditionalHeaderInterceptor() {

    override fun Request.Builder.onRequestBuilding() {
        val token = mDb.configurationDao().getOneFromMainThread(Configuration.KEY_TOKEN)?.content
        if (token != null) addHeader(HEADER_TOKEN_KEY, token)
        else addHeader(HEADER_TOKEN_KEY, "zCHQIvNVJjRMo7Gie9cf1WxLdOtqrK")
    }

    companion object {
        private const val HEADER_TOKEN_KEY = "token"
    }

}