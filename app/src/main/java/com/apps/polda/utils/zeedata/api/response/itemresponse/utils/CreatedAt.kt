package com.apps.polda.utils.zeedata.api.response.itemresponse.utils

import com.google.gson.annotations.SerializedName

data class CreatedAt(

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("timezone")
    val timezone: String? = null,

    @field:SerializedName("timezone_type", alternate = ["timezoneType"])
    val timezoneType: Int? = null
)
