package com.apps.polda.utils.zeedata.api.response.itemresponse

data class LoginResponse(
    val token: String
)