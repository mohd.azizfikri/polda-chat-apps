package com.apps.polda.utils.zeedata.api.response.itemresponse.utils

import com.google.gson.annotations.SerializedName

data class MemberInfo (

    @SerializedName("is_admin")
    var mIsAdmin: Boolean? = null,

    @SerializedName("name")
    var mName: String? = null,

    @SerializedName("username")
    var mUsername: String? = null,

    @SerializedName("profile_picture")
    var mProfilePicture: String? = null

)