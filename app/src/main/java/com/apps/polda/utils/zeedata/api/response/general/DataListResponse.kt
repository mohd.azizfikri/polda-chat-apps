package com.apps.polda.utils.zeedata.api.response.general

import com.google.gson.annotations.SerializedName

/**
 * Data of lists
 */
data class DataListResponse<T>(
    @field:SerializedName("status") val status: Int = 0,
    @field:SerializedName("statusDesc") val statusDesc: String = "",
    @field:SerializedName("data") val data: List<T>? = null
)