package com.apps.polda.utils.zeecommon.ui

interface HasObservers {

    fun setupObservers()

}