package com.apps.polda.feature.panduan

import android.app.Activity
import androidx.activity.viewModels
import com.apps.polda.R
import com.apps.polda.databinding.ActivityPanduanDetailBinding
import com.apps.polda.feature.main.MainActivity
import com.apps.polda.utils.zeecommon.extension.startActivity
import com.apps.polda.utils.zeecommon.extension.startActivityAsNewTask
import com.apps.polda.utils.zeecommon.ui.AppActivity
import com.apps.polda.utils.zeecommon.ui.HasObservers
import com.apps.polda.utils.zeecommon.ui.HasViews
import com.apps.polda.utils.zeedata.api.response.itemresponse.PanduanResponse
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PanduanDetailActivity : AppActivity<ActivityPanduanDetailBinding, PanduanDetailViewModel>
    (R.layout.activity_panduan_detail), HasViews, HasObservers {

    override val viewModel by viewModels<PanduanDetailViewModel>()

    private val mStartData by lazy {
        Gson().fromJson(intent?.getStringExtra(EXTRA_DATA_PANDUAN),
            PanduanResponse::class.java)
    }

    override fun setupViews() {
        viewBinding.ltoolbar.setOnPrimaryIconClick {
            finish()
        }
        viewModel.setup(mStartData.title, mStartData.description)
    }

    override fun setupObservers() {
        // TODO("Not yet implemented")
    }

    companion object {
        private const val EXTRA_DATA_PANDUAN = "EXTRA_DATA_PANDUAN"
        fun launch(activity: Activity, extraIdPanduan: String) {
            activity.startActivity<PanduanDetailActivity> {
                putExtra(EXTRA_DATA_PANDUAN, extraIdPanduan)
            }
        }
    }

}