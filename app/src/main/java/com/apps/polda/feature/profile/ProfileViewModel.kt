package com.apps.polda.feature.profile

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.apps.polda.utils.zeecommon.ui.AppViewModel
import com.apps.polda.utils.zeecommon.util.event.LiveEvent
import com.apps.polda.utils.zeecommon.util.event.MutableLiveEvent
import com.apps.polda.utils.zeedata.api.response.itemresponse.FriendResponse
import com.apps.polda.utils.zeedata.api.response.itemresponse.ProfileResponse
import com.apps.polda.utils.zeedata.repository.ProfileRepository
import com.natpryce.onFailure
import kotlinx.coroutines.launch

class ProfileViewModel @ViewModelInject constructor(
    private val profileRepository: ProfileRepository
): AppViewModel() {

    private val _onCLickEditProfileEvent by lazy { MutableLiveEvent<ProfileResponse>() }
    val onCLickEditProfileEvent: LiveEvent<ProfileResponse> = _onCLickEditProfileEvent

    private val _onSuccessLogoutEvent by lazy { MutableLiveEvent<Unit>() }
    val onSuccessLogoutEvent: LiveEvent<Unit> = _onSuccessLogoutEvent

    val nama by lazy { MutableLiveData("") }
    val notelp by lazy { MutableLiveData("") }
    val image by lazy { MutableLiveData<String>() }

    val profile by lazy { MutableLiveData<ProfileResponse>() }
    val isMe by lazy { MutableLiveData(true) }

    fun loadData(data: FriendResponse){
        nama.value = data.name
        notelp.value = data.username

        isMe.value = false
    }

    fun loadData(){
        isMe.value = true

        viewModelScope.launch {
            val result = profileRepository.getProfile().onFailure {
                _showToastMessageEvent.set(it.reason.localizedMessage ?: "Connection in trouble!")
                Log.w("Error", it.reason.localizedMessage.orEmpty() +" - OR -  "+ it.toString())
                return@launch
            }
            profile.value = result
            nama.value = result.name
            notelp.value = result.username
            result.profile_picture?.let {
                image.value = it
            }
        }
    }

    fun openEditProfile(){
        profile.value?.let { _onCLickEditProfileEvent.set(it) }
    }

    fun clickogout() {
        viewModelScope.launch {
            profileRepository.logout().onFailure {
                _showToastMessageEvent.set(it.reason.localizedMessage ?: "Connection in trouble!")
                Log.w("Error", it.reason.localizedMessage.orEmpty() +" - OR -  "+ it.toString())
                return@launch
            }
            _onSuccessLogoutEvent.set(Unit)
        }
    }
}