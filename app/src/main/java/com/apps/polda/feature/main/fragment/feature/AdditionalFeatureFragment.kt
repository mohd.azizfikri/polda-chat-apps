package com.apps.polda.feature.main.fragment.feature

import android.widget.PopupMenu
import androidx.fragment.app.viewModels
import com.apps.polda.R
import com.apps.polda.databinding.FragmentAdditionalFeatureBinding
import com.apps.polda.feature.contact.FriendRequestActivity
import com.apps.polda.feature.panduan.PanduanListActivity
import com.apps.polda.feature.profile.ProfileActivity
import com.apps.polda.feature.video.VideoListActivity
import com.apps.polda.utils.zeecommon.extension.startActivity
import com.apps.polda.utils.zeecommon.ui.AppFragment
import com.apps.polda.utils.zeecommon.ui.HasObservers
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AdditionalFeatureFragment : AppFragment<FragmentAdditionalFeatureBinding, AdditionalFeatureViewModel>
    (R.layout.fragment_additional_feature), HasObservers {

    override val viewModel by viewModels <AdditionalFeatureViewModel>()

    override fun setupObservers() {
        viewModel.openMoreMenuEvent.observe(this) {
            val popupmenu = PopupMenu(requireContext(), viewBinding.btnOption)
            popupmenu.menuInflater.inflate(R.menu.option_menu, popupmenu.menu)
            popupmenu.setOnMenuItemClickListener { item ->
                when(item.itemId){
                    R.id.friend_request -> {
                        requireActivity().startActivity<FriendRequestActivity>()
                    }
                    R.id.setting ->{
                        ProfileActivity.launch(requireActivity())
                    }
                }
                true
            }
            popupmenu.show()
        }
        viewModel.openPanduanMenuEvent.observe(this) {
            requireActivity().startActivity<PanduanListActivity>()
        }
        viewModel.openVideoMenuEvent.observe(this) {
            requireActivity().startActivity<VideoListActivity>()
        }
    }
}