package com.apps.polda.feature.profile

import android.app.Activity
import androidx.activity.viewModels
import com.apps.polda.R
import com.apps.polda.databinding.ActivityProfileBinding
import com.apps.polda.feature.auth.login.LoginActivity
import com.apps.polda.utils.zeecommon.extension.startActivity
import com.apps.polda.utils.zeecommon.extension.startActivityAsNewTask
import com.apps.polda.utils.zeecommon.ui.AppActivity
import com.apps.polda.utils.zeecommon.ui.HasObservers
import com.apps.polda.utils.zeecommon.ui.HasViews
import com.apps.polda.utils.zeedata.api.response.itemresponse.FriendResponse
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProfileActivity : AppActivity<ActivityProfileBinding, ProfileViewModel>
    (R.layout.activity_profile), HasViews, HasObservers {
    override val viewModel by viewModels <ProfileViewModel>()

    private val extraData by lazy { intent.getStringExtra(EXTRA_DATA_PROFILE) }

    override fun setupViews() {
        extraData?.let {
            viewModel.loadData(
                Gson().fromJson(it, FriendResponse::class.java)
            )
        } ?: viewModel.loadData()

        viewBinding.ltoolbar.setOnPrimaryIconClick {
            finish()
        }
    }

    override fun setupObservers() {
        viewModel.onCLickEditProfileEvent.observe(this) {
            when(it.profile_picture.isNullOrEmpty()){
                true -> UpdateProfileActivity.launch(this, it.name.orEmpty(), it.username.orEmpty())
                else -> UpdateProfileActivity.launch(this, it.name.orEmpty(), it.username.orEmpty(), it.profile_picture)
            }
        }
        viewModel.onSuccessLogoutEvent.observe(this){
            startActivityAsNewTask<LoginActivity>()
            finish()
        }
    }

    companion object {
        private const val EXTRA_DATA_PROFILE = "EXTRA_DATA_PROFILE"
        fun launch(activity: Activity, extraData: String) {
            activity.startActivity<ProfileActivity> {
                putExtra(EXTRA_DATA_PROFILE, extraData)
            }
        }
        fun launch(activity: Activity) {
            activity.startActivity<ProfileActivity>()
        }
    }
}