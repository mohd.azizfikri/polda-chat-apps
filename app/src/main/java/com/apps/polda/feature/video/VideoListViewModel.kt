package com.apps.polda.feature.video

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.apps.polda.common.adapter.feature.ListVideoAdapter
import com.apps.polda.utils.zeecommon.SelectableData
import com.apps.polda.utils.zeecommon.extension.toFalse
import com.apps.polda.utils.zeecommon.extension.toTrue
import com.apps.polda.utils.zeecommon.ui.AppViewModel
import com.apps.polda.utils.zeecommon.util.event.LiveEvent
import com.apps.polda.utils.zeecommon.util.event.MutableLiveEvent
import com.apps.polda.utils.zeecommon.util.list.AppDataWrapper
import com.apps.polda.utils.zeedata.api.response.itemresponse.VideoResponse
import com.apps.polda.utils.zeedata.repository.CommonRepository
import com.natpryce.Success
import com.natpryce.onFailure
import kotlinx.coroutines.launch

class VideoListViewModel @ViewModelInject constructor(
    private val mCommonRepository: CommonRepository
) : AppViewModel(), ListVideoAdapter.VideoItemListener {
    private val _changeIconToSearchEvent by lazy { MutableLiveEvent<Unit>() }
    val changeIconToSearchEvent: LiveEvent<Unit> = _changeIconToSearchEvent

    private val _onCLickItemVideoEvent by lazy { MutableLiveEvent<VideoResponse>() }
    val onCLickItemVideoEvent: LiveEvent<VideoResponse> = _onCLickItemVideoEvent

    val videoListWrapper by lazy { AppDataWrapper<SelectableData<VideoResponse>>() }
    val keyword by lazy { MutableLiveData("") }
    val isSearchOpen by lazy { MutableLiveData(false) }

    init {
        fetchList()
    }

    fun fetchList() {
        keyword.value = ""
        isSearchOpen.value = false
        _changeIconToSearchEvent.set(Unit)

        viewModelScope.launch {
            loading.toTrue()
            videoListWrapper.load {
                val results = mCommonRepository.getVideo().onFailure {
                    loading.toFalse()
                    _showToastMessageEvent.set(it.reason.localizedMessage ?: "Connection in trouble!")
                    Log.w("Error", it.reason.localizedMessage.orEmpty() +" - OR -  "+ it.toString())
                    return@load Success(emptyList())
                }
                loading.toFalse()
                Success(results.map { SelectableData(origin = it) })
            }
        }
    }

    fun search(){
        viewModelScope.launch {
            loading.toTrue()
            videoListWrapper.load {
                val results = mCommonRepository.searchVideo(keyword.value.orEmpty()).onFailure {
                    loading.toFalse()
                    _showToastMessageEvent.set(it.reason.localizedMessage ?: "Connection in trouble!")
                    Log.w("Error", it.reason.localizedMessage.orEmpty() +" - OR -  "+ it.toString())
                    return@load Success(emptyList())
                }
                loading.toFalse()
                Success(results.map { SelectableData(origin = it) })
            }
        }
    }

    override fun onClickVideoItem(data: VideoResponse) {
        _onCLickItemVideoEvent.set(data)
    }
}