package com.apps.polda.feature.contact.information

import android.app.Activity
import android.content.Intent
import androidx.activity.viewModels
import com.apps.polda.R
import com.apps.polda.common.adapter.contact.MemberGroupAdapter
import com.apps.polda.databinding.ActivityGroupInfoBinding
import com.apps.polda.utils.zeecommon.ui.AppActivity
import com.apps.polda.utils.zeecommon.ui.HasObservers
import com.apps.polda.utils.zeecommon.ui.HasViews
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GroupInfoActivity : AppActivity<ActivityGroupInfoBinding, GroupInfoViewModel>
    (R.layout.activity_group_info), HasViews, HasObservers {

    override val viewModel by viewModels<GroupInfoViewModel>()

    private val mGroupMemberAdapter by lazy { MemberGroupAdapter(viewModel) }

    private val mIdGroup by lazy { intent.getStringExtra(ID_GROUP) }

    override fun setupViews() {
        viewModel.setup(mIdGroup)

        viewBinding.ltoolbar.setOnPrimaryIconClick {
            finish()
        }

        viewBinding.rvContactlist.adapter = mGroupMemberAdapter
    }

    override fun setupObservers() {
        viewModel.openDetailProfileEvent.observe(this) {
            FriendInfoActivity.launch(this, it.mUsername.orEmpty())
        }
        viewModel.openImageActivityEvent.observe(this){
            LargeImageActivity.launch(this, LargeImageActivity.EXTRA_FILE_TYPE_URL, it)
        }
    }

    companion object {
        const val ID_GROUP = "ID_GROUP"
        fun launch(activity: Activity, idGroup: String) {
            activity.startActivity(Intent(activity, GroupInfoActivity::class.java).apply {
                putExtra(ID_GROUP, idGroup)
            })
        }
    }
}