package com.apps.polda.feature.splash

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.apps.polda.utils.zeecommon.ui.AppViewModel
import com.apps.polda.utils.zeecommon.util.event.LiveEvent
import com.apps.polda.utils.zeecommon.util.event.MutableLiveEvent
import com.apps.polda.utils.zeedata.repository.ProfileRepository
import com.natpryce.onFailure
import kotlinx.coroutines.launch

class SplashScreenViewModel @ViewModelInject constructor(
    private val mUserRepository: ProfileRepository
) : AppViewModel() {

    private val _alreadyLoginEvent by lazy { MutableLiveEvent<Boolean>() }
    val alreadyLoginEvent: LiveEvent<Boolean> = _alreadyLoginEvent

    private val _didntHaveProvinceEvent by lazy { MutableLiveEvent<String>() }
    val didntHaveProvinceEvent: LiveEvent<String> = _didntHaveProvinceEvent

    init {
        validateToken()
    }

    private fun validateToken() {
        viewModelScope.launch {
            val result = mUserRepository.validateToken().onFailure { return@launch }

            when(result.status){
                10 -> {
                    result.data?.let {
                        when(it.is_has_province){
                        true -> _alreadyLoginEvent.set(true)
                        else -> _didntHaveProvinceEvent.set(it.name.orEmpty())
                    } } ?: _alreadyLoginEvent.set(true)
                }
                else -> _alreadyLoginEvent.set(false)
            }
        }
    }
}