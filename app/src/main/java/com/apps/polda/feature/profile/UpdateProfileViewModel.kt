package com.apps.polda.feature.profile

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.apps.polda.utils.zeecommon.extension.toFalse
import com.apps.polda.utils.zeecommon.extension.toTrue
import com.apps.polda.utils.zeecommon.ui.AppViewModel
import com.apps.polda.utils.zeecommon.util.event.LiveEvent
import com.apps.polda.utils.zeecommon.util.event.MutableLiveEvent
import com.apps.polda.utils.zeedata.repository.ProfileRepository
import kotlinx.coroutines.launch
import java.io.File

class UpdateProfileViewModel @ViewModelInject constructor(
    private val profileRepository: ProfileRepository
) : AppViewModel() {
    private val _openImagePickerEvent by lazy { MutableLiveEvent<Unit>() }
    val openImagePickerEvent: LiveEvent<Unit> = _openImagePickerEvent

    private val _onChoiceProvinceEvent by lazy { MutableLiveEvent<Unit>() }
    val onChoiceProvinceEvent: LiveEvent<Unit> = _onChoiceProvinceEvent

    private val _finishEditEvent by lazy { MutableLiveEvent<Unit>() }
    val finishEditEvent: LiveEvent<Unit> = _finishEditEvent

    val pageType by lazy { MutableLiveData(0) }

    val nama by lazy { MutableLiveData("") }
    val nomor_hp by lazy { MutableLiveData("") }
    val provinsi by lazy { MutableLiveData("") }
    val file by lazy { MutableLiveData<File>() }

    fun setup(
        cNama: String? = "",
        cPhone: String? = "",
        cType: Int? = 0,
        cProvinc: String? = "Pilih Provinsi"
    ){
        pageType.value = cType

        nama.value = cNama
        nomor_hp.value = cPhone
        provinsi.value = cProvinc
    }

    fun openProvinceDialog(){
        _onChoiceProvinceEvent.set(Unit)
    }

    fun openImagePicker(){
        _openImagePickerEvent.set(Unit)
    }

    fun clickSubmit(){
        if(pageType.value == 0){
            when {
                nama.value.isNullOrEmpty() ->  _showToastMessageEvent.set("Nama Wajib diisi")
                provinsi.value.equals("Pilih Provinsi") -> _showToastMessageEvent.set("Provinsi Wajib diisi")
                else -> proceedSubmit()
            }
        }else{
            when {
                nama.value.isNullOrEmpty() ->  _showToastMessageEvent.set("Nama Wajib diisi")
                nomor_hp.value.isNullOrEmpty() -> _showToastMessageEvent.set("Nomor HP Wajib diisi")
                else -> proceedSubmit()
            }
        }

    }

    private fun proceedSubmit(){
        viewModelScope.launch {
            loading.toTrue()
            when (pageType.value) {
                0 -> {
                    profileRepository.registProfile(provinsi.value.orEmpty(), file.value)
                    _finishEditEvent.set(Unit)
                }
                else -> {
                    profileRepository.updateProfile(nama.value.orEmpty(), file.value)
                    _finishEditEvent.set(Unit)
                }
            }
            loading.toFalse()
        }
    }
}