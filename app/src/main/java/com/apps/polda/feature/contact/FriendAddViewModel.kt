package com.apps.polda.feature.contact

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.apps.polda.common.adapter.contact.FriendAddAdapter
import com.apps.polda.utils.zeecommon.SelectableData
import com.apps.polda.utils.zeecommon.extension.toFalse
import com.apps.polda.utils.zeecommon.extension.toTrue
import com.apps.polda.utils.zeecommon.ui.AppViewModel
import com.apps.polda.utils.zeecommon.util.event.LiveEvent
import com.apps.polda.utils.zeecommon.util.event.MutableLiveEvent
import com.apps.polda.utils.zeecommon.util.list.AppDataWrapper
import com.apps.polda.utils.zeedata.api.response.itemresponse.FriendResponse
import com.apps.polda.utils.zeedata.repository.ContactRepository
import com.natpryce.Success
import com.natpryce.onFailure
import kotlinx.coroutines.launch

class FriendAddViewModel @ViewModelInject constructor(
    private val mContactRepository: ContactRepository
) : AppViewModel(), FriendAddAdapter.ListFriendItemListener {
    private val _openDetailProfileEvent by lazy { MutableLiveEvent<FriendResponse>() }
    val openDetailProfileEvent: LiveEvent<FriendResponse> = _openDetailProfileEvent

    private val _addAsFriendSuccessEvent by lazy { MutableLiveEvent<FriendResponse>() }
    val addAsFriendSuccessEvent: LiveEvent<FriendResponse> = _addAsFriendSuccessEvent

    val friendAddWrapper by lazy { AppDataWrapper<SelectableData<FriendResponse>>() }
    val keyword by lazy { MutableLiveData("") }


    fun fetchList() {
        if(keyword.value?.length == 0){
            _showToastMessageEvent.set("Mohon masukkan username atau nomor yang dicari")
        }else{
            connectServerFetchList()
        }
    }

    fun connectServerFetchList(){
        viewModelScope.launch {
            friendAddWrapper.load {
                loading.toTrue()
                val results = mContactRepository.searchUsername(keyword.value.orEmpty()).onFailure {
                    loading.toFalse()
                    _showToastMessageEvent.set(it.reason.localizedMessage ?: "Connection trouble!")
                    return@load Success(emptyList())
                }
                loading.toFalse()
                Success(results.map { SelectableData(origin = it) })
            }
        }
    }

    override fun onClickListFriendItem(data: FriendResponse) {
        _openDetailProfileEvent.set(data)
    }

    override fun onClickAddAsFriend(data: FriendResponse) {
        viewModelScope.launch {
            data.id?.let { mContactRepository.sendReqAddFriend(it.toString()).onFailure { error ->
                _showToastMessageEvent.set(error.reason.localizedMessage ?: "Connection trouble!")
                return@launch
            }
                _showToastMessageEvent.set("Berhasil mengirim permintaan pertemanan ke ${data.name} ")
                _addAsFriendSuccessEvent.set(data)
            }
        }
    }
}