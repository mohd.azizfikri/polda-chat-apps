package com.apps.polda.feature.auth.login

import android.app.Activity
import android.content.Intent
import androidx.activity.viewModels
import com.apps.polda.R
import com.apps.polda.databinding.ActivityLoginBinding
import com.apps.polda.feature.auth.register.RegisterActivity
import com.apps.polda.feature.main.MainActivity
import com.apps.polda.utils.zeecommon.extension.startActivity
import com.apps.polda.utils.zeecommon.extension.startActivityAsNewTask
import com.apps.polda.utils.zeecommon.ui.AppActivity
import com.apps.polda.utils.zeecommon.ui.HasObservers
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : AppActivity<ActivityLoginBinding, LoginViewModel>
    (R.layout.activity_login), HasObservers {
    override val viewModel by viewModels<LoginViewModel>()

    override fun setupObservers() {
        viewModel.openRegisterPageEvent.observe(this) {
            startActivity(Intent(this, RegisterActivity::class.java))
        }
        viewModel.loginSuccessEvent.observe(this) {
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_MULTIPLE_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }
    }

    companion object {
        fun launch(activity: Activity) {
            activity.startActivity(Intent(activity, LoginActivity::class.java))
        }
    }

}