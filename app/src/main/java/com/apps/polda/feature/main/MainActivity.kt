package com.apps.polda.feature.main

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.apps.polda.R
import com.apps.polda.databinding.ActivityMainBinding
import com.apps.polda.feature.auth.login.LoginActivity
import com.apps.polda.feature.auth.otp.OTPVerificationActivity
import com.apps.polda.feature.main.fragment.feature.AdditionalFeatureFragment
import com.apps.polda.feature.main.fragment.groupchat.GroupChatFragment
import com.apps.polda.feature.main.fragment.privatechat.PrivateChatFragment
import com.apps.polda.utils.zeecommon.extension.startActivity
import com.apps.polda.utils.zeecommon.extension.startActivityAsNewTask
import com.apps.polda.utils.zeecommon.ui.AppActivity
import com.apps.polda.utils.zeecommon.ui.HasBackPressedConfirmation
import com.apps.polda.utils.zeecommon.ui.HasObservers
import com.apps.polda.utils.zeecommon.ui.HasViews
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.initialization.InitializationStatus
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener
import com.google.firebase.iid.internal.FirebaseInstanceIdInternal
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppActivity<ActivityMainBinding, MainViewModel>
    (R.layout.activity_main), HasViews, HasObservers,
    HasBackPressedConfirmation, FirebaseInstanceIdInternal.NewTokenListener {

    override val viewModel by viewModels<MainViewModel>()

    override val backPressDelay: Long = 2000

    private val mStartupPage by lazy { intent?.getSerializableExtra(EXTRA_STARTUP) as? Page }

    private val mPages by lazy {
        mapOf<Page, () -> Fragment>(
            Page.PRIVATE to { PrivateChatFragment() },
            Page.GROUP to { GroupChatFragment() },
            Page.FEATURED to { AdditionalFeatureFragment() }
        )
    }
    private val REQUESTED_PERMISSIONS = arrayOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    private fun checkPermission() {
        if(checkSelfPermission(REQUESTED_PERMISSIONS[0], 1) &&
            checkSelfPermission(REQUESTED_PERMISSIONS[1], 2)){
        }
    }

    private fun checkSelfPermission(permission: String, requestCode: Int): Boolean {
        if (ContextCompat.checkSelfPermission(this, permission) !==
            PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this, REQUESTED_PERMISSIONS, requestCode)
            return false
        }
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.registerFCM()
        checkPermission()
    }

    override fun onNewToken(token: String) {
        Log.d("MainActivity", "Refreshed token: $token")
        viewModel.sendTokenToServer(token)
    }

    override fun setupViews() {
        // TODO Fragmented Page
        viewBinding.bnvMain.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_private -> showPage(Page.PRIVATE)
                R.id.nav_group -> viewModel.openOrSignIn { showPage(Page.GROUP) }
                R.id.nav_featured -> viewModel.openOrSignIn { showPage(Page.FEATURED) }
                else -> false
            }
        }
        mStartupPage?.let { viewBinding.bnvMain.selectedItemId = getMenuIdFromPage(it) } ?: showPage(Page.PRIVATE)

        // TODO Advertising
        MobileAds.initialize(this) { }
        val adRequest = AdRequest.Builder().build()
        viewBinding.adView.loadAd(adRequest)
    }

    override fun setupObservers() {
        // TODO("Not yet implemented")
        viewModel.openIntroPageEvent.observe(this) {
            LoginActivity.launch(this)
        }
        viewModel.openOtpPageEvent.observe(this) {
            startActivityAsNewTask<OTPVerificationActivity>()
        }
    }

    private fun getMenuIdFromPage(page: Page): Int {
        return when (page) {
            Page.PRIVATE -> R.id.nav_private
            Page.GROUP -> R.id.nav_group
            Page.FEATURED -> R.id.nav_featured
        }
    }

    private fun showPage(page: Page): Boolean {
        mPages[page]?.let { fragment ->
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fl_main, fragment())
                .commit()
            return true
        }
        return false
    }

    enum class Page { PRIVATE, GROUP, FEATURED }

    companion object {
        private const val EXTRA_STARTUP = "EXTRA_STARTUP"
        fun launch(activity: Activity, asNewTask: Boolean = false, startup: Page = Page.PRIVATE) {
            if (asNewTask) activity.startActivityAsNewTask<MainActivity>() {
                putExtra(
                    EXTRA_STARTUP,
                    startup
                )
            }
            else activity.startActivity<MainActivity> {
                putExtra(EXTRA_STARTUP, startup)
            }
        }
    }
}