package com.apps.polda.feature.auth.otp

import android.os.CountDownTimer
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.apps.polda.utils.zeecommon.ui.AppViewModel
import com.apps.polda.utils.zeecommon.util.event.LiveEvent
import com.apps.polda.utils.zeecommon.util.event.MutableLiveEvent
import com.apps.polda.utils.zeedata.repository.ProfileRepository
import com.natpryce.onFailure
import kotlinx.coroutines.launch

class OTPVerificationViewModel @ViewModelInject constructor(
    private val profileRepository: ProfileRepository
) : AppViewModel(){

    private val _validationSuccessEvent by lazy { MutableLiveEvent<Unit>() }
    val validationSuccessEvent: LiveEvent<Unit> = _validationSuccessEvent

    val code by lazy { MutableLiveData("") }
    val isSuccess by lazy { MutableLiveData(false) }
    val isError by lazy { MutableLiveData(false) }

    fun clickSubmit(){
        viewModelScope.launch {
            if(code.value?.length == 6){
                profileRepository.otpvalidation(code.value.orEmpty()).onFailure {
                    isError.value = true
                    runCountDown()
                    return@launch
                }

                isSuccess.value = true
                runCountDown()
            }
        }
    }

    private fun runCountDown(){
        _showToastMessageEvent.set("Error : ${isError.value}  Sukses : ${isSuccess.value}")

        val timer = object: CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}

            override fun onFinish() {
                when (true){
                    isError.value -> {
                        isError.value = false
                        code.value = ""
                    }
                    isSuccess.value -> _validationSuccessEvent.set(Unit)
                }
            }
        }
        timer.start()
    }
}