package com.apps.polda.feature.contact.information

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.apps.polda.common.adapter.contact.MemberGroupAdapter
import com.apps.polda.utils.zeecommon.SelectableData
import com.apps.polda.utils.zeecommon.extension.orNol
import com.apps.polda.utils.zeecommon.extension.toFalse
import com.apps.polda.utils.zeecommon.extension.toTrue
import com.apps.polda.utils.zeecommon.ui.AppViewModel
import com.apps.polda.utils.zeecommon.util.event.LiveEvent
import com.apps.polda.utils.zeecommon.util.event.MutableLiveEvent
import com.apps.polda.utils.zeecommon.util.list.AppDataWrapper
import com.apps.polda.utils.zeedata.api.response.itemresponse.utils.MemberInfo
import com.apps.polda.utils.zeedata.repository.ContactRepository
import com.natpryce.Success
import com.natpryce.onFailure
import kotlinx.coroutines.launch

class GroupInfoViewModel @ViewModelInject constructor(
    private val mContactRepository: ContactRepository
) : AppViewModel(), MemberGroupAdapter.ListMemberGroupListener {
    private val _openDetailProfileEvent by lazy { MutableLiveEvent<MemberInfo>() }
    val openDetailProfileEvent: LiveEvent<MemberInfo> = _openDetailProfileEvent

    private val _openImageActivityEvent by lazy { MutableLiveEvent<String>() }
    val openImageActivityEvent: LiveEvent<String> = _openImageActivityEvent

    private val mGroupID by lazy { MutableLiveData("") }
    val mGroupName by lazy { MutableLiveData("") }
    val mGroupPicture by lazy { MutableLiveData("") }
    val mGroupTotalMember by lazy { MutableLiveData("") }
    
    val memberListWrapper by lazy { AppDataWrapper<SelectableData<MemberInfo>>() }

    fun setup(idGroup: String? = null){
        idGroup?.let {
            mGroupID.value = idGroup
            fetchList()
        } ?: _showToastMessageEvent.set("Informasi Provinsi anda tidak ditemukan")
    }

    fun fetchList(){
        viewModelScope.launch {
            loading.toTrue()
            val results = mContactRepository.getGroupInfo(mGroupID.value.orEmpty()).onFailure {
                loading.toFalse()
                _showToastMessageEvent.set(it.reason.localizedMessage ?: "Connection trouble!")
                return@launch
            }
            mGroupName.value = results.mName
            mGroupPicture.value = results.mProfilePicture
            mGroupTotalMember.value = results.mTotalMember.orNol().toString() + " Anggota"

            memberListWrapper.load {
                loading.toFalse()
                Success(results.mMemberInfo?.let { list ->
                    list.map { SelectableData(origin = it) }
                } ?: emptyList())
            }
        }
    }

    fun openImage(){
        mGroupPicture.value?.let { _openImageActivityEvent.set(it) }
    }

    override fun onClickMemberGroup(data: MemberInfo) {
        _openDetailProfileEvent.set(data)
    }
}