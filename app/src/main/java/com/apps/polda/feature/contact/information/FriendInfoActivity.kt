package com.apps.polda.feature.contact.information

import android.app.Activity
import android.content.Intent
import androidx.activity.viewModels
import com.apps.polda.R
import com.apps.polda.databinding.ActivityFriendInfoBinding
import com.apps.polda.utils.zeecommon.ui.AppActivity
import com.apps.polda.utils.zeecommon.ui.HasObservers
import com.apps.polda.utils.zeecommon.ui.HasViews
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FriendInfoActivity : AppActivity<ActivityFriendInfoBinding, FriendInfoViewModel>
    (R.layout.activity_friend_info), HasViews, HasObservers {

    override val viewModel by viewModels<FriendInfoViewModel>()

    private val mIdFriend by lazy { intent.getStringExtra(ID_USER) }

    override fun setupViews() {
        viewModel.setup(mIdFriend)

        viewBinding.ltoolbar.setOnPrimaryIconClick {
            finish()
        }
    }

    companion object {
        const val ID_USER = "ID_USER"
        fun launch(activity: Activity, idFriend: String) {
            activity.startActivity(Intent(activity, FriendInfoActivity::class.java).apply {
                putExtra(ID_USER, idFriend)
            })
        }
    }

    override fun setupObservers() {
        viewModel.openImageActivityEvent.observe(this){
            LargeImageActivity.launch(this, LargeImageActivity.EXTRA_FILE_TYPE_URL, it)
        }
    }
}