package com.apps.polda.feature.panduan

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.apps.polda.utils.zeecommon.ui.AppViewModel

class PanduanDetailViewModel @ViewModelInject constructor(

) : AppViewModel() {
    val title by lazy { MutableLiveData("") }
    val description by lazy { MutableLiveData("") }

    fun setup(sTitle: String? = null, sDescription: String? = null ) {
        title.value = sTitle ?: "Sample Title"
        description.value = sDescription ?: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Non curabitur gravida arcu ac tortor dignissim. Eu tincidunt tortor aliquam nulla facilisi cras fermentum odio. Pellentesque pulvinar pellentesque habitant morbi tristique senectus et netus. Ultrices tincidunt arcu non sodales. Elementum nibh tellus molestie nunc non blandit massa enim. Nunc mi ipsum faucibus vitae aliquet nec ullamcorper sit amet. Odio morbi quis commodo odio aenean sed adipiscing diam. Cursus metus aliquam eleifend mi in. Eu facilisis sed odio morbi quis commodo odio aenean sed. Eu mi bibendum neque egestas congue quisque egestas. Nisi est sit amet facilisis magna etiam tempor orci. Eleifend donec pretium vulputate sapien nec sagittis. Blandit aliquam etiam erat velit scelerisque in dictum.\n" +
                "\n" +
                "Donec ultrices tincidunt arcu non sodales neque sodales ut. Lectus vestibulum mattis ullamcorper velit. Quis enim lobortis scelerisque fermentum. Et molestie ac feugiat sed lectus. Eget felis eget nunc lobortis mattis aliquam faucibus purus. Erat pellentesque adipiscing commodo elit at imperdiet dui. Cum sociis natoque penatibus et magnis. Sed risus ultricies tristique nulla aliquet. Orci phasellus egestas tellus rutrum tellus pellentesque eu tincidunt. Mauris rhoncus aenean vel elit scelerisque. Enim diam vulputate ut pharetra. Leo integer malesuada nunc vel. Nisi lacus sed viverra tellus in hac. Eu volutpat odio facilisis mauris sit. Faucibus ornare suspendisse sed nisi lacus sed viverra tellus. Nunc id cursus metus aliquam eleifend. Donec ultrices tincidunt arcu non sodales. Posuere urna nec tincidunt praesent semper feugiat. Sit amet venenatis urna cursus eget nunc scelerisque viverra. Libero volutpat sed cras ornare arcu."
    }
}