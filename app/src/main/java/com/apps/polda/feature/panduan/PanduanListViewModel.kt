package com.apps.polda.feature.panduan

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.apps.polda.common.adapter.feature.ListPanduanAdapter
import com.apps.polda.utils.zeecommon.SelectableData
import com.apps.polda.utils.zeecommon.extension.toFalse
import com.apps.polda.utils.zeecommon.extension.toTrue
import com.apps.polda.utils.zeecommon.ui.AppViewModel
import com.apps.polda.utils.zeecommon.util.event.LiveEvent
import com.apps.polda.utils.zeecommon.util.event.MutableLiveEvent
import com.apps.polda.utils.zeecommon.util.list.AppDataWrapper
import com.apps.polda.utils.zeedata.api.response.itemresponse.PanduanResponse
import com.apps.polda.utils.zeedata.repository.CommonRepository
import com.natpryce.Success
import com.natpryce.onFailure
import kotlinx.coroutines.launch

class PanduanListViewModel @ViewModelInject constructor(
    private val mCommonRepository: CommonRepository
) : AppViewModel(), ListPanduanAdapter.PanduanItemListener {
    private val _changeIconToSearchEvent by lazy { MutableLiveEvent<Unit>() }
    val changeIconToSearchEvent: LiveEvent<Unit> = _changeIconToSearchEvent

    private val _onCLickItemPanduanEvent by lazy { MutableLiveEvent<PanduanResponse>() }
    val onCLickItemPanduanEvent: LiveEvent<PanduanResponse> = _onCLickItemPanduanEvent

    val panduanListWrapper by lazy { AppDataWrapper<SelectableData<PanduanResponse>>() }
    val keyword by lazy { MutableLiveData("") }
    val isSearchOpen by lazy { MutableLiveData(false) }

    init {
        fetchList()
    }

    fun fetchList() {
        keyword.value = ""
        isSearchOpen.value = false
        _changeIconToSearchEvent.set(Unit)

        viewModelScope.launch {
            loading.toTrue()
            panduanListWrapper.load {
                val results = mCommonRepository.getPanduan().onFailure {
                    loading.toFalse()
                    _showToastMessageEvent.set(it.reason.localizedMessage ?: "Connection in trouble!")
                    Log.w("Error", it.reason.localizedMessage.orEmpty() +" - OR -  "+ it.toString())
                    return@load Success(emptyList())
                }
                loading.toFalse()
                Success(results.map { SelectableData(origin = it) })
            }
        }
    }

    fun search(){
        viewModelScope.launch {
            loading.toTrue()
            panduanListWrapper.load {
                val results = mCommonRepository.getPanduan(keyword.value.orEmpty()).onFailure {
                    loading.toFalse()
                    _showToastMessageEvent.set(it.reason.localizedMessage ?: "Connection in trouble!")
                    Log.w("Error", it.reason.localizedMessage.orEmpty() +" - OR -  "+ it.toString())
                    return@load Success(emptyList())
                }
                loading.toFalse()
                Success(results.map { SelectableData(origin = it) })
            }
        }
    }

    override fun onClickPanduanItem(data: PanduanResponse) {
        _onCLickItemPanduanEvent.set(data)
    }

}