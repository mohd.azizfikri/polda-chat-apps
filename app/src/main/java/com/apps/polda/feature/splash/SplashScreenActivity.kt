package com.apps.polda.feature.splash

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.apps.polda.R
import com.apps.polda.databinding.ActivitySplashScreenBinding
import com.apps.polda.feature.auth.login.LoginActivity
import com.apps.polda.feature.main.MainActivity
import com.apps.polda.feature.profile.UpdateProfileActivity
import com.apps.polda.utils.zeecommon.extension.startActivityAsNewTask
import com.apps.polda.utils.zeecommon.ui.AppActivity
import com.apps.polda.utils.zeecommon.ui.HasObservers
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashScreenActivity : AppActivity<ActivitySplashScreenBinding, SplashScreenViewModel>
    (R.layout.activity_splash_screen), HasObservers {

    override val viewModel by viewModels<SplashScreenViewModel>()

    private val REQUESTED_PERMISSIONS = arrayOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    private fun checkPermission() {
        if(checkSelfPermission(REQUESTED_PERMISSIONS[0], 1) &&
            checkSelfPermission(REQUESTED_PERMISSIONS[1], 2)){
        }
    }

    private fun checkSelfPermission(permission: String, requestCode: Int): Boolean {
        if (ContextCompat.checkSelfPermission(this, permission) !==
            PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this, REQUESTED_PERMISSIONS, requestCode)
            return false
        }
        return true
    }

    override fun setupObservers() {
        checkPermission()

        viewModel.alreadyLoginEvent.observe(this){
            val timer = object: CountDownTimer(6000, 1000) {
                override fun onTick(millisUntilFinished: Long) {}

                override fun onFinish() {
                    if(it) startActivity(Intent(applicationContext, MainActivity::class.java))
                    else startActivity(Intent(applicationContext, LoginActivity::class.java))
                    finish()
                }
            }
            timer.start()
        }
        viewModel.didntHaveProvinceEvent.observe(this){
            val timer = object: CountDownTimer(6000, 1000) {
                override fun onTick(millisUntilFinished: Long) {}

                override fun onFinish() {
                    UpdateProfileActivity.launch(this@SplashScreenActivity, it)
                    finish()
                }
            }
            timer.start()
        }
    }
}