package com.apps.polda.feature.contact.information

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import coil.load
import com.apps.polda.R
import com.apps.polda.feature.panduan.PanduanDetailActivity
import com.apps.polda.utils.zeecommon.binding.loadFile
import com.apps.polda.utils.zeecommon.binding.loadUrl
import com.apps.polda.utils.zeecommon.extension.startActivity
import kotlinx.android.synthetic.main.activity_large_image.*
import java.io.File

class LargeImageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_large_image)

        ltoolbar.setOnPrimaryIconClick {
            finish()
        }

        val file = intent.getStringExtra(EXTRA_FILE)

        when(intent.getIntExtra(EXTRA_FILE_TYPE, EXTRA_FILE_TYPE_URL)){
            EXTRA_FILE_TYPE_PATH -> {
                large_imageview.loadFile(File(file))
            }
            EXTRA_FILE_TYPE_URL -> {
                large_imageview.loadUrl(file)
            }
        }
    }

    companion object {
        private const val EXTRA_FILE = "EXTRA_FILE"
        private const val EXTRA_FILE_TYPE = "EXTRA_FILE_TYPE"
        const val EXTRA_FILE_TYPE_URL = 10000000
        const val EXTRA_FILE_TYPE_PATH = 200000000
        fun launch(activity: Activity, fileType: Int, file: String) {
            activity.startActivity<LargeImageActivity> {
                putExtra(EXTRA_FILE_TYPE, fileType)
                putExtra(EXTRA_FILE, file)
            }
        }
    }
}
