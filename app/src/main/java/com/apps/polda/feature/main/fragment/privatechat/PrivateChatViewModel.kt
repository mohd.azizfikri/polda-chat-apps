package com.apps.polda.feature.main.fragment.privatechat

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.apps.polda.common.adapter.chat.ChatListAdapter
import com.apps.polda.utils.zeecommon.SelectableData
import com.apps.polda.utils.zeecommon.extension.mutateList
import com.apps.polda.utils.zeecommon.extension.toFalse
import com.apps.polda.utils.zeecommon.extension.toTrue
import com.apps.polda.utils.zeecommon.ui.AppViewModel
import com.apps.polda.utils.zeecommon.util.event.LiveEvent
import com.apps.polda.utils.zeecommon.util.event.MutableLiveEvent
import com.apps.polda.utils.zeecommon.util.list.AppDataWrapper
import com.apps.polda.utils.zeedata.api.response.itemresponse.ChatResponse
import com.apps.polda.utils.zeedata.repository.ChatRepository
import com.natpryce.Success
import com.natpryce.onFailure
import kotlinx.coroutines.launch

class PrivateChatViewModel @ViewModelInject constructor(
    private val mChatRepository: ChatRepository
) : AppViewModel(), ChatListAdapter.ChatItemListener {
    private val _openMoreMenuEvent by lazy { MutableLiveEvent<Unit>() }
    val openMoreMenuEvent: LiveEvent<Unit> = _openMoreMenuEvent

    private val _openDeleteDialogEvent by lazy { MutableLiveEvent<ChatResponse>() }
    val openDeleteDialogEvent: LiveEvent<ChatResponse> = _openDeleteDialogEvent

    private val _openContactListEvent by lazy { MutableLiveEvent<Unit>() }
    val openContactListEvent: LiveEvent<Unit> = _openContactListEvent

    private val _openChattEvent by lazy { MutableLiveEvent<ChatResponse>() }
    val openChattEvent: LiveEvent<ChatResponse> = _openChattEvent

    private val _onRefreshListAdapterEvent by lazy { MutableLiveEvent<ChatResponse>() }
    val onRefreshListAdapterEvent: LiveEvent<ChatResponse> = _onRefreshListAdapterEvent

    val chatItemListWrapper by lazy { AppDataWrapper<ChatResponse>() }

    val searchKeyword by lazy { MutableLiveData("") }
    val isSearchMode by lazy { MutableLiveData(false) }

    init {
        fetchChatList()
    }

    fun fetchChatList(){
        isSearchMode.value = false
        searchKeyword.value = ""

        viewModelScope.launch {
            chatItemListWrapper.load {
                loading.toTrue()
                val results = mChatRepository.getListPrivateChat().onFailure {
                    _showToastMessageEvent.set(it.reason.localizedMessage ?: "Connection Error ")
                    loading.toFalse()
                    return@load Success(emptyList())
                }
                loading.toFalse()
                Success(results)
            }
        }
    }

    fun search(bool: Boolean){
        when(bool){
            true -> {
                if(searchKeyword.value.isNullOrEmpty()){
                    isSearchMode.value = false
                }else {
                    viewModelScope.launch {
                        chatItemListWrapper.load {
                            loading.toTrue()
                            val results =
                                mChatRepository.getListPrivateChat(searchKeyword.value.orEmpty())
                                    .onFailure {
                                        _showToastMessageEvent.set(
                                            it.reason.localizedMessage ?: "Connection Error "
                                        )
                                        loading.toFalse()
                                        return@load Success(emptyList())
                                    }
                            loading.toFalse()
                            Success(results)
                        }
                    }
                }
            }
            false -> {
                isSearchMode.value = true
            }
        }
    }

    fun openMenu(){
        _openMoreMenuEvent.set(Unit)
    }

    fun createNewMessage(){
        _openContactListEvent.set(Unit)
    }

    override fun onClickChatItem(data: ChatResponse) {
        if(!data.selected){
            _openChattEvent.set(data)
        }
        showDeleteButton(data, false)
    }

    override fun onHoldChatItem(data: ChatResponse) {
        showDeleteButton(data)
    }

    override fun onClickDeleteChatItem(data: ChatResponse) {
        _openDeleteDialogEvent.set(data)
    }

    override fun onClickCancelSelectedItem(data: ChatResponse) {
        showDeleteButton(data, false)
    }

    fun deleteChatItem(data: ChatResponse){
        viewModelScope.launch {
            loading.toTrue()
            data.destUsername?.let {
                mChatRepository.deleteChatFrom(it)
                    .onFailure { it1 ->
                        _showToastMessageEvent.set(
                            it1.reason.localizedMessage ?: "Connection Error "
                        )
                        loading.toFalse()
                        return@launch
                    }
            }
            _showToastMessageEvent.set("Pesan dari ${data.name} berhasil dihapus")
            fetchChatList()
        }
    }

    fun showDeleteButton(data: ChatResponse, setValue: Boolean? = null){
        chatItemListWrapper.liveData.value = chatItemListWrapper.liveData.value?.map { it.selected = false; it }
        data.selected = setValue ?: !data.selected
        chatItemListWrapper.liveData.mutateList(data) { item -> data.destUsername == item.destUsername }

        _onRefreshListAdapterEvent.set(data)
    }

}