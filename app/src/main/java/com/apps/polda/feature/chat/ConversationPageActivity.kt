package com.apps.polda.feature.chat

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.view.LayoutInflater
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import com.apps.polda.R
import com.apps.polda.common.adapter.chat.ChatBubbleListAdapter
import com.apps.polda.databinding.ActivityConversationPageBinding
import com.apps.polda.feature.contact.information.FriendInfoActivity
import com.apps.polda.feature.contact.information.GroupInfoActivity
import com.apps.polda.utils.zeecommon.extension.startActivity
import com.apps.polda.utils.zeecommon.ui.AppActivity
import com.apps.polda.utils.zeecommon.ui.HasObservers
import com.apps.polda.utils.zeecommon.ui.HasViews
import com.apps.polda.utils.zeewidget.ZeePlacePicker
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_conversation_page.*
import kotlinx.android.synthetic.main.dialog_attach_choices.view.*

@AndroidEntryPoint
class ConversationPageActivity : AppActivity<ActivityConversationPageBinding, ConversationPageViewModel>
    (R.layout.activity_conversation_page), HasViews, HasObservers/*, NotifBroadcastReceiver.MyBroadcastListener*/ {
    override val viewModel by viewModels<ConversationPageViewModel>()

    private val mChatBubbleListAdapter by lazy { ChatBubbleListAdapter() }

    private val mChatId by lazy { intent.getStringExtra(EXTRA_CHAT_ID) }
    private val mTitle by lazy { intent.getStringExtra(EXTRA_CHAT_SENDER) }
    private val mIsGroup by lazy { intent.getBooleanExtra(EXTRA_IS_GROUP, false) }

    private val mLatLng by lazy { MutableLiveData<LatLng>() }

    private val REQUESTED_PERMISSIONS = arrayOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    override fun setupViews() {
        mChatId?.let { viewModel.setup(mTitle ?: "Message", it, mIsGroup) }
            ?: finish()

        viewBinding.ltoolbar.setOnPrimaryIconClick {
            finish()
        }
        viewBinding.ltoolbar.setOnSecondaryIconClick {
            viewModel.launchConversation()
        }

        ltoolbar.setOnClickListener {
            if(mIsGroup){
                GroupInfoActivity.launch(this, mChatId.orEmpty())
            }else{
                FriendInfoActivity.launch(this, mChatId.orEmpty())
            }
        }

        viewBinding.rvChatConversation.adapter = mChatBubbleListAdapter

        checkPermission()
        setupLocation()
    }


    private fun setupLocation(){
        val mFusedLocation = LocationServices.getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        mFusedLocation.lastLocation.addOnSuccessListener {
            mLatLng.value = try {
                LatLng(it.latitude, it.longitude)
            }catch (e: Exception){
                LatLng(-6.1266751, 106.626638)
            }
        }
    }

    private fun checkPermission() {
        if(checkSelfPermission(REQUESTED_PERMISSIONS[0], 1) &&
            checkSelfPermission(REQUESTED_PERMISSIONS[1], 2)){
        }
    }

    private fun checkSelfPermission(permission: String, requestCode: Int): Boolean {
        if (ContextCompat.checkSelfPermission(this, permission) !==
            PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this, REQUESTED_PERMISSIONS, requestCode)
            return false
        }
        return true
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun setupObservers() {
        viewModel.openAttachImageEvent.observe(this) {
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_attach_choices, null)
            val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
                .setTitle("Choose")
            val  mAlertDialog = mBuilder.show()
            mDialogView.location_choice.setOnClickListener {
                mAlertDialog.dismiss()
                startActivityForResult(
                    Intent(applicationContext, ZeePlacePicker::class.java).apply {
                        putExtra("LAT", mLatLng.value!!.latitude)
                        putExtra("LNG", mLatLng.value!!.longitude)

                    }, ADDRESS_PICKER_REQUEST
                )
            }
            mDialogView.image_choice.setOnClickListener {
                mAlertDialog.dismiss()
                ImagePicker
                    .with(this)
                    .cropSquare()
                    .start(RC_IMAGE_PICKER)
            }
        }
        viewModel.onSuccessSendEvent.observe(this) {
            viewBinding.textSend.text.clear()
            mChatBubbleListAdapter.notifyDataSetChanged()
        }
        viewModel.scrollToEndEvent.observe(this) {
            viewBinding.rvChatConversation.scrollToPosition(viewBinding.rvChatConversation.adapter?.itemCount?.minus(
                2
            ) ?: 0)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_IMAGE_PICKER) {
            val imageFile = ImagePicker.getFile(data)
            if (imageFile != null) {
                viewModel.uploadImage(imageFile)
            }
        }else if(requestCode == ADDRESS_PICKER_REQUEST) {
            if(resultCode == RESULT_OK){
                val lat = data!!.getDoubleExtra("LAT",-6.9034443)
                val lng = data.getDoubleExtra("LNG", 107.5731162)

                viewModel.uploadLocation("${lat},${lng}")
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    companion object {
        const val EXTRA_CHAT_SENDER = "EXTRA_CHAT_SENDER"
        const val EXTRA_CHAT_ID = "EXTRA_CHAT_ID"
        const val EXTRA_IS_GROUP = "EXTRA_IS_GROUP"

        private const val RC_IMAGE_PICKER = 56
        private const val ADDRESS_PICKER_REQUEST = 705
        fun launch(activity: Activity, sender: String, chatId: String) {
            activity.startActivity<ConversationPageActivity> {
                putExtra(EXTRA_CHAT_SENDER, sender)
                putExtra(EXTRA_CHAT_ID, chatId)
            }
        }
        fun launch(activity: Activity, sender: String, chatId: String, isGroup: Boolean) {
            activity.startActivity<ConversationPageActivity> {
                putExtra(EXTRA_CHAT_SENDER, sender)
                putExtra(EXTRA_CHAT_ID, chatId)
                putExtra(EXTRA_IS_GROUP, isGroup)
            }
        }
    }

}