package com.apps.polda.feature.video

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import com.apps.polda.R
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerView
import kotlinx.android.synthetic.main.activity_video_embeded.*

class VideoEmbededActivity : YouTubeBaseActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_embeded)

        val link: String? = intent?.getStringExtra("LINK_YOUTUBE")
        val author: String? = intent?.getStringExtra("AUTHOR")
        val title: String? = intent?.getStringExtra("TITLE")
        val videoId = link?.split("v=")?.get(1)

        textTitle.text = title
        textAuthor.text = "Author: $author"

        Log.e("Link videoId", videoId.orEmpty())

        val yPal = findViewById<YouTubePlayerView>(R.id.youtubePlayer)
        yPal.initialize(getString(R.string.id_gcp_video), object : YouTubePlayer.OnInitializedListener{
            override fun onInitializationSuccess(
                p0: YouTubePlayer.Provider?,
                p1: YouTubePlayer?,
                p2: Boolean
            ) {
                //TODO("Not yet implemented")
                p1!!.loadVideo(videoId)
                p1.play()
            }

            override fun onInitializationFailure(
                p0: YouTubePlayer.Provider?,
                p1: YouTubeInitializationResult?
            ) {
                //TODO("Not yet implemented")

            }
        })


        // TODO Advertising
        MobileAds.initialize(this) { }
        val adRequest = AdRequest.Builder().build()
        ad_view.loadAd(adRequest)
    }
}