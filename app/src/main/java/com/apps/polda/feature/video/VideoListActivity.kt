package com.apps.polda.feature.video

import android.content.Intent
import androidx.activity.viewModels
import com.apps.polda.R
import com.apps.polda.common.adapter.feature.ListVideoAdapter
import com.apps.polda.databinding.ActivityVideoListBinding
import com.apps.polda.utils.zeecommon.extension.startActivity
import com.apps.polda.utils.zeecommon.extension.toast
import com.apps.polda.utils.zeecommon.ui.AppActivity
import com.apps.polda.utils.zeecommon.ui.HasObservers
import com.apps.polda.utils.zeecommon.ui.HasViews
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class VideoListActivity : AppActivity<ActivityVideoListBinding, VideoListViewModel>
    (R.layout.activity_video_list), HasViews, HasObservers {

    override val viewModel by viewModels<VideoListViewModel>()
    private val mVideoAdapter by lazy { ListVideoAdapter(viewModel) }

    override fun setupViews() {
        viewBinding.ltoolbar.setOnPrimaryIconClick {
            finish()
        }
        viewBinding.ltoolbar.setOnSecondaryIconClick {
            viewModel.isSearchOpen.value = viewModel.isSearchOpen.value?.let {
                when(it) {
                    false -> {
                        viewBinding.ltoolbar.setSecondaryIconImage(R.drawable.ic_close)
                        true
                    }
                    else -> {
                        viewModel.fetchList()
                        viewBinding.ltoolbar.setSecondaryIconImage(R.drawable.ic_search_white)
                        false
                    }
                }
            }
        }

        viewBinding.rvVideo.adapter = mVideoAdapter

    }

    override fun setupObservers() {
        viewModel.onCLickItemVideoEvent.observe(this) {
            val intent = Intent(this, VideoEmbededActivity::class.java)
            intent.putExtra("LINK_YOUTUBE", it.link)
            intent.putExtra("AUTHOR", it.author)
            intent.putExtra("TITLE", it.title)
            startActivity(intent)

        }
        viewModel.changeIconToSearchEvent.observe(this) {
            viewBinding.ltoolbar.setSecondaryIconImage(R.drawable.ic_search_white)
        }
    }
}