package com.apps.polda.feature.contact

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.apps.polda.common.adapter.contact.FriendListAdapter
import com.apps.polda.utils.zeecommon.SelectableData
import com.apps.polda.utils.zeecommon.extension.toFalse
import com.apps.polda.utils.zeecommon.extension.toTrue
import com.apps.polda.utils.zeecommon.ui.AppViewModel
import com.apps.polda.utils.zeecommon.util.event.LiveEvent
import com.apps.polda.utils.zeecommon.util.event.MutableLiveEvent
import com.apps.polda.utils.zeecommon.util.list.AppDataWrapper
import com.apps.polda.utils.zeedata.api.response.itemresponse.FriendResponse
import com.apps.polda.utils.zeedata.repository.ContactRepository
import com.natpryce.Success
import com.natpryce.onFailure
import kotlinx.coroutines.launch

class FriendListViewModel @ViewModelInject constructor(
    private val mContactRepository: ContactRepository
) : AppViewModel(), FriendListAdapter.ListFriendItemListener {
    private val _openChatPageEvent by lazy { MutableLiveEvent<FriendResponse>() }
    val openChatPageEvent: LiveEvent<FriendResponse> = _openChatPageEvent

    val friendListWrapper by lazy { AppDataWrapper<SelectableData<FriendResponse>>() }

    init {
        fetchList()
    }

    fun fetchList() {
        viewModelScope.launch {
            friendListWrapper.load {
                loading.toTrue()
                val results = mContactRepository.getFriendList().onFailure {
                    loading.toFalse()
                    return@load Success(emptyList())
                }
                loading.toFalse()
                Success(results.map { SelectableData(origin = it) })
            }
        }
    }

    override fun onClickListFriendItem(data: FriendResponse) {
        _openChatPageEvent.set(data)
    }
}