package com.apps.polda.feature.contact

import android.annotation.SuppressLint
import androidx.activity.viewModels
import com.apps.polda.R
import com.apps.polda.common.adapter.SampleAdapter
import com.apps.polda.common.adapter.SampleListener
import com.apps.polda.common.adapter.contact.FriendRequestAdapter
import com.apps.polda.databinding.ActivityFriendRequestBinding
import com.apps.polda.feature.profile.ProfileActivity
import com.apps.polda.utils.sampleList
import com.apps.polda.utils.zeecommon.extension.startActivity
import com.apps.polda.utils.zeecommon.ui.AppActivity
import com.apps.polda.utils.zeecommon.ui.HasObservers
import com.apps.polda.utils.zeecommon.ui.HasViews
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FriendRequestActivity : AppActivity<ActivityFriendRequestBinding, FriendRequestViewModel>
    (R.layout.activity_friend_request), HasViews, HasObservers {
    override val viewModel by viewModels<FriendRequestViewModel>()

    private val mFriendRequestAdapter by lazy { FriendRequestAdapter(viewModel) }

    override fun setupViews() {
        viewBinding.ltoolbar.setOnPrimaryIconClick {
            finish()
        }

        viewBinding.rvFriendRequest.adapter = mFriendRequestAdapter
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun setupObservers() {
        viewModel.openProfilePageEvent.observe(this) {
            ProfileActivity.launch(this, Gson().toJson(it))
        }
        viewModel.onActionSelectedEvent.observe(this) {
            mFriendRequestAdapter.notifyDataSetChanged()
        }
    }

}