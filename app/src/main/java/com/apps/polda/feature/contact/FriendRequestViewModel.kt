package com.apps.polda.feature.contact

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.apps.polda.common.adapter.contact.FriendRequestAdapter
import com.apps.polda.utils.zeecommon.SelectableData
import com.apps.polda.utils.zeecommon.extension.toFalse
import com.apps.polda.utils.zeecommon.extension.toTrue
import com.apps.polda.utils.zeecommon.ui.AppViewModel
import com.apps.polda.utils.zeecommon.util.event.LiveEvent
import com.apps.polda.utils.zeecommon.util.event.MutableLiveEvent
import com.apps.polda.utils.zeecommon.util.list.AppDataWrapper
import com.apps.polda.utils.zeedata.api.response.itemresponse.FriendResponse
import com.apps.polda.utils.zeedata.repository.ContactRepository
import com.natpryce.Success
import com.natpryce.onFailure
import kotlinx.coroutines.launch

class FriendRequestViewModel @ViewModelInject constructor(
    private val mContactRepository: ContactRepository
) : AppViewModel(), FriendRequestAdapter.FriendRequestItemListener {
    private val _openProfilePageEvent by lazy { MutableLiveEvent<FriendResponse>() }
    val openProfilePageEvent: LiveEvent<FriendResponse> = _openProfilePageEvent

    private val _onActionSelectedEvent by lazy { MutableLiveEvent<FriendResponse>() }
    val onActionSelectedEvent: LiveEvent<FriendResponse> = _onActionSelectedEvent

    val friendRequestListWrapper by lazy { AppDataWrapper<SelectableData<FriendResponse>>() }

    init {
        fetchList()
    }

    fun fetchList() {
        viewModelScope.launch {
            friendRequestListWrapper.load {
                loading.toTrue()
                val results = mContactRepository.getFriendReqList().onFailure {
                    loading.toFalse()
                    return@load Success(emptyList())
                }
                loading.toFalse()
                Success(results.map { SelectableData(origin = it) })
            }
        }
    }

    override fun onClickFriendRequestItem(data: FriendResponse) {
        _openProfilePageEvent.set(data)
    }

    override fun onActionAcceptFriendRequest(data: FriendResponse) {
        viewModelScope.launch {
            loading.toTrue()
            data.id?.let {
                mContactRepository.postActionFriendReq(it, 1).onFailure { error ->
                    _showToastMessageEvent.set("Sorry " + error.reason.localizedMessage)
                    loading.toFalse()
                    return@launch
                }
            }
            loading.toFalse()
            _showToastMessageEvent.set("Kamu sekarang berteman dengan " + data.name)
            _onActionSelectedEvent.set(data)
        }
    }

    override fun onActionRejectFriendRequest(data: FriendResponse) {
        viewModelScope.launch {
            loading.toTrue()
            data.id?.let {
                mContactRepository.postActionFriendReq(it, 0).onFailure { error ->
                    _showToastMessageEvent.set("Sorry " + error.reason.localizedMessage)
                    loading.toFalse()
                    return@launch
                }
            }
            loading.toFalse()
            _onActionSelectedEvent.set(data)
        }
    }

}