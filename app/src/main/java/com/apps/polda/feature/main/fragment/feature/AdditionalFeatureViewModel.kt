package com.apps.polda.feature.main.fragment.feature

import androidx.hilt.lifecycle.ViewModelInject
import com.apps.polda.utils.zeecommon.ui.AppViewModel
import com.apps.polda.utils.zeecommon.util.event.LiveEvent
import com.apps.polda.utils.zeecommon.util.event.MutableLiveEvent

class AdditionalFeatureViewModel @ViewModelInject constructor(

) : AppViewModel() {
    private val _openMoreMenuEvent by lazy { MutableLiveEvent<Unit>() }
    val openMoreMenuEvent: LiveEvent<Unit> = _openMoreMenuEvent

    private val _openPanduanMenuEvent by lazy { MutableLiveEvent<Unit>() }
    val openPanduanMenuEvent: LiveEvent<Unit> = _openPanduanMenuEvent

    private val _openVideoMenuEvent by lazy { MutableLiveEvent<Unit>() }
    val openVideoMenuEvent: LiveEvent<Unit> = _openVideoMenuEvent

    fun openMenu() {
        _openMoreMenuEvent.set(Unit)
    }

    fun openTabPanduan() {
        _openPanduanMenuEvent.set(Unit)
    }

    fun openTabVideo() {
        _openVideoMenuEvent.set(Unit)
    }
}