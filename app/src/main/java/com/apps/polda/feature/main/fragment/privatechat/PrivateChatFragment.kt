package com.apps.polda.feature.main.fragment.privatechat

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.widget.PopupMenu
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.apps.polda.R
import com.apps.polda.common.adapter.chat.ChatListAdapter
import com.apps.polda.databinding.FragmentPrivateChatBinding
import com.apps.polda.feature.chat.ConversationPageActivity
import com.apps.polda.feature.contact.FriendListActivity
import com.apps.polda.feature.contact.FriendRequestActivity
import com.apps.polda.feature.main.MainViewModel
import com.apps.polda.feature.profile.ProfileActivity
import com.apps.polda.utils.zeecommon.extension.startActivity
import com.apps.polda.utils.zeecommon.ui.AppFragment
import com.apps.polda.utils.zeecommon.ui.HasObservers
import com.apps.polda.utils.zeecommon.ui.HasParentViewModel
import com.apps.polda.utils.zeecommon.ui.HasViews
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PrivateChatFragment : AppFragment<FragmentPrivateChatBinding, PrivateChatViewModel>
    (R.layout.fragment_private_chat), HasParentViewModel, HasViews, HasObservers {

    override val parentViewModel by activityViewModels<MainViewModel>()
    override val viewModel by viewModels <PrivateChatViewModel>()

    private val mChatListAdapter by lazy { ChatListAdapter(viewModel) }

    override fun onResume() {
        super.onResume()
        viewModel.fetchChatList()
    }

    override fun setupViews() {
        viewBinding.rvPrivateChat.adapter = mChatListAdapter
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun setupObservers() {
        viewModel.openMoreMenuEvent.observe(this) {
            val popupmenu = PopupMenu(requireContext(), viewBinding.btnOption)
            popupmenu.menuInflater.inflate(R.menu.option_menu, popupmenu.menu)

            popupmenu.setOnMenuItemClickListener { item ->
                when(item.itemId){
                    R.id.friend_request -> {
                        requireActivity().startActivity<FriendRequestActivity>()
                    }
                    R.id.setting -> {
                        ProfileActivity.launch(requireActivity())
                    }
                }
                true
            }
            popupmenu.show()
        }
        viewModel.openDeleteDialogEvent.observe(this) {
            val alertDialogBuilder = AlertDialog.Builder(requireContext())
            alertDialogBuilder.setMessage("Apakah anda ingin menghapus percakapan dengan `${it.name}`?");
            alertDialogBuilder.setPositiveButton(
                "Tidak"
            ) { _, _ ->
                viewModel.showDeleteButton(it, false)
            }
            alertDialogBuilder.setNegativeButton(
                "Ya"
            ) { _, _ ->
                viewModel.deleteChatItem(it)
            }

            val alertDialog = alertDialogBuilder.create()
            alertDialog.show()
        }

        viewModel.openContactListEvent.observe(this) {
            requireActivity().startActivity<FriendListActivity>()
        }
        viewModel.openChattEvent.observe(this) {
            ConversationPageActivity.launch(requireActivity(), it.name.orEmpty(), it.destUsername.orEmpty())
        }
        viewModel.onRefreshListAdapterEvent.observe(this) {
            mChatListAdapter.notifyDataSetChanged()
        }
    }

}