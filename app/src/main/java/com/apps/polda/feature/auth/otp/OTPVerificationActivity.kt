package com.apps.polda.feature.auth.otp

import android.app.Activity
import android.content.Intent
import androidx.activity.viewModels
import com.apps.polda.R
import com.apps.polda.databinding.ActivityOtpVerificationBinding
import com.apps.polda.feature.auth.login.LoginActivity
import com.apps.polda.feature.main.MainActivity
import com.apps.polda.utils.zeecommon.extension.startActivity
import com.apps.polda.utils.zeecommon.extension.startActivityAsNewTask
import com.apps.polda.utils.zeecommon.ui.AppActivity
import com.apps.polda.utils.zeecommon.ui.HasObservers
import com.apps.polda.utils.zeecommon.ui.HasViews
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OTPVerificationActivity : AppActivity<ActivityOtpVerificationBinding, OTPVerificationViewModel>
    (R.layout.activity_otp_verification), HasObservers, HasViews {
    override val viewModel by viewModels<OTPVerificationViewModel>()

    override fun setupObservers() {
        viewModel.validationSuccessEvent.observe(this) {
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_MULTIPLE_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }
    }

    override fun setupViews() {
        // TODO("Not yet implemented")
    }

    companion object {
        fun launch(activity: Activity, asNewTask: Boolean = false) {
            if (asNewTask) activity.startActivityAsNewTask<OTPVerificationActivity>()
            else  activity.startActivity(Intent(activity, OTPVerificationActivity::class.java))
        }
    }
}