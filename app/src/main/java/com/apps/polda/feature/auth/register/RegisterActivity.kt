package com.apps.polda.feature.auth.register

import android.content.Intent
import androidx.activity.viewModels
import com.apps.polda.R
import com.apps.polda.databinding.ActivityRegisterBinding
import com.apps.polda.feature.auth.otp.OTPVerificationActivity
import com.apps.polda.feature.main.MainActivity
import com.apps.polda.feature.profile.UpdateProfileActivity
import com.apps.polda.utils.zeecommon.ui.AppActivity
import com.apps.polda.utils.zeecommon.ui.HasObservers
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterActivity : AppActivity<ActivityRegisterBinding, RegisterViewModel>
    (R.layout.activity_register), HasObservers {
    override val viewModel by viewModels<RegisterViewModel>()

    override fun setupObservers() {
        viewModel.openVerificationPageEvent.observe(this){
//            startActivity(Intent(this, OTPVerificationActivity::class.java))
//            startActivity(Intent(this, MainActivity::class.java))
            UpdateProfileActivity.launch(this, it)
        }
    }
}