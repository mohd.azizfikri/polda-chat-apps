package com.apps.polda.feature.contact

import androidx.activity.viewModels
import com.apps.polda.R
import com.apps.polda.common.adapter.contact.FriendListAdapter
import com.apps.polda.databinding.ActivityFriendListBinding
import com.apps.polda.feature.chat.ConversationPageActivity
import com.apps.polda.utils.zeecommon.extension.startActivity
import com.apps.polda.utils.zeecommon.ui.AppActivity
import com.apps.polda.utils.zeecommon.ui.HasObservers
import com.apps.polda.utils.zeecommon.ui.HasViews
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FriendListActivity : AppActivity<ActivityFriendListBinding, FriendListViewModel>
    (R.layout.activity_friend_list), HasViews, HasObservers {

    override val viewModel by viewModels<FriendListViewModel>()

    private val mFriendListAdapter by lazy { FriendListAdapter(viewModel) }

    override fun setupViews() {
        viewBinding.ltoolbar.setOnPrimaryIconClick {
            finish()
        }

        viewBinding.ltoolbar.setOnSecondaryIconClick {
            startActivity<FriendAddActivity>()
        }

        viewBinding.rvContactlist.adapter = mFriendListAdapter
    }

    override fun setupObservers() {
        viewModel.openChatPageEvent.observe(this) {
            ConversationPageActivity.launch(this, it.name.orEmpty(), it.username.orEmpty())
        }
    }
}