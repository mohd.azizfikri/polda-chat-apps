package com.apps.polda.feature.main.fragment.groupchat

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.apps.polda.common.adapter.chat.ChatListAdapter
import com.apps.polda.utils.zeecommon.extension.toFalse
import com.apps.polda.utils.zeecommon.extension.toTrue
import com.apps.polda.utils.zeecommon.ui.AppViewModel
import com.apps.polda.utils.zeecommon.util.event.LiveEvent
import com.apps.polda.utils.zeecommon.util.event.MutableLiveEvent
import com.apps.polda.utils.zeecommon.util.list.AppDataWrapper
import com.apps.polda.utils.zeedata.api.response.itemresponse.ChatResponse
import com.apps.polda.utils.zeedata.repository.ChatRepository
import com.natpryce.Success
import com.natpryce.onFailure
import kotlinx.coroutines.launch

class GroupChatViewModel @ViewModelInject constructor(
    private val mChatRepository: ChatRepository
): AppViewModel(), ChatListAdapter.ChatItemListener {
    private val _openMoreMenuEvent by lazy { MutableLiveEvent<Unit>() }
    val openMoreMenuEvent: LiveEvent<Unit> = _openMoreMenuEvent

    private val _openChattEvent by lazy { MutableLiveEvent<ChatResponse>() }
    val openChattEvent: LiveEvent<ChatResponse> = _openChattEvent

    val chatItemListWrapper by lazy { AppDataWrapper<ChatResponse>() }

    val searchKeyword by lazy { MutableLiveData("") }
    val isSearchMode by lazy { MutableLiveData(false) }

    init {
        fetchChatList()
    }

    fun fetchChatList(){
        viewModelScope.launch {
            chatItemListWrapper.load {
                loading.toTrue()
                val results = mChatRepository.getListGroupChat().onFailure {
                    _showToastMessageEvent.set(it.reason.localizedMessage ?: "Connection Error ")
                    loading.toFalse()
                    return@load Success(emptyList())
                }
                loading.toFalse()
                Success(results)
            }
        }
    }

    fun search(bool: Boolean){
        when(bool){
            true -> {
                isSearchMode.value = false
            }
            false -> {
                isSearchMode.value = true
            }
        }
    }

    fun openMenu(){
        _openMoreMenuEvent.set(Unit)
    }

    override fun onClickChatItem(data: ChatResponse) {
        _openChattEvent.set(data)
    }

    override fun onHoldChatItem(data: ChatResponse) {
        // TODO (Only used at private chat)
    }

    override fun onClickDeleteChatItem(data: ChatResponse) {
        // TODO (Only used at private chat)
    }

    override fun onClickCancelSelectedItem(data: ChatResponse) {
        // TODO (Only used at private chat)
    }

}