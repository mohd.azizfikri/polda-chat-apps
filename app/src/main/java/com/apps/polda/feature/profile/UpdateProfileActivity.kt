package com.apps.polda.feature.profile

import android.app.Activity
import android.content.Intent
import androidx.activity.viewModels
import com.apps.polda.R
import com.apps.polda.common.dialog.ProvinceDialog
import com.apps.polda.databinding.ActivityUpdateProfileBinding
import com.apps.polda.feature.main.MainActivity
import com.apps.polda.utils.zeecommon.binding.loadAnimatedCircleFile
import com.apps.polda.utils.zeecommon.binding.loadAnimatedCircleUrl
import com.apps.polda.utils.zeecommon.extension.startActivity
import com.apps.polda.utils.zeecommon.ui.AppActivity
import com.apps.polda.utils.zeecommon.ui.HasObservers
import com.apps.polda.utils.zeecommon.ui.HasViews
import com.github.dhaval2404.imagepicker.ImagePicker
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UpdateProfileActivity : AppActivity<ActivityUpdateProfileBinding, UpdateProfileViewModel>
    (R.layout.activity_update_profile), HasObservers, HasViews {
    override val viewModel by viewModels <UpdateProfileViewModel>()

    private val extraName by lazy { intent.getStringExtra(EXTRA_DATA_NAME) }
    private val extraPhone by lazy { intent.getStringExtra(EXTRA_DATA_PHONE) }
    private val extraImage by lazy { intent.getStringExtra(EXTRA_DATA_IMAGE) }
    private val extraType by lazy { intent.getIntExtra(EXTRA_DATA_TYPE, 0) }

    override fun setupViews() {
        extraImage?.let {
            viewBinding.imageProfile.loadAnimatedCircleUrl(it)
        }
        viewModel.setup(extraName, extraPhone, extraType)
    }

    override fun setupObservers() {
        viewModel.onChoiceProvinceEvent.observe(this) {
            ProvinceDialog { data: String ->
                viewModel.provinsi.value = data
            }.show(supportFragmentManager, ProvinceDialog::class.java.canonicalName)
        }
        viewModel.openImagePickerEvent.observe(this) {
            ImagePicker
                .with(this)
                .cropSquare()
                .start(10010)
        }
        viewModel.finishEditEvent.observe(this){
            startActivity<MainActivity>()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 10010){
            val imageFile = ImagePicker.getFile(data)
            if (imageFile != null) {
                viewModel.file.value = imageFile
                viewBinding.imageProfile.loadAnimatedCircleFile(imageFile)
            }
        }
    }

    companion object {
        private const val EXTRA_DATA_NAME = "EXTRA_DATA_NAME"
        private const val EXTRA_DATA_PHONE = "EXTRA_DATA_PHONE"
        private const val EXTRA_DATA_IMAGE = "EXTRA_DATA_IMAGE"
        private const val EXTRA_DATA_TYPE = "EXTRA_DATA_TYPE"

        fun launch(activity: Activity, extraName: String) {
            activity.startActivity<UpdateProfileActivity> {
                putExtra(EXTRA_DATA_NAME, extraName)
                putExtra(EXTRA_DATA_TYPE, 0)
            }
        }
        fun launch(activity: Activity, extraName: String, extraPhone: String, extraImage: String) {
            activity.startActivity<UpdateProfileActivity> {
                putExtra(EXTRA_DATA_NAME, extraName)
                putExtra(EXTRA_DATA_PHONE, extraPhone)
                putExtra(EXTRA_DATA_IMAGE, extraImage)
                putExtra(EXTRA_DATA_TYPE, 1)
            }
        }
        fun launch(activity: Activity, extraName: String, extraPhone: String) {
            activity.startActivity<UpdateProfileActivity> {
                putExtra(EXTRA_DATA_NAME, extraName)
                putExtra(EXTRA_DATA_PHONE, extraPhone)
                putExtra(EXTRA_DATA_TYPE, 1)
            }
        }
    }
}