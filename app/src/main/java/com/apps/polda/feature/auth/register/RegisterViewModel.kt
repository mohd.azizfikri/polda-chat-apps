package com.apps.polda.feature.auth.register

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.apps.polda.utils.zeecommon.ui.AppViewModel
import com.apps.polda.utils.zeecommon.util.event.LiveEvent
import com.apps.polda.utils.zeecommon.util.event.MutableLiveEvent
import com.apps.polda.utils.zeedata.repository.ProfileRepository
import com.natpryce.onFailure
import kotlinx.coroutines.launch

class RegisterViewModel @ViewModelInject constructor(
    private val profileRepository: ProfileRepository
): AppViewModel() {

    private val _openVerificationPageEvent by lazy { MutableLiveEvent<String>() }
    val openVerificationPageEvent: LiveEvent<String> = _openVerificationPageEvent

    val email by lazy { MutableLiveData("") }
    val phonenumber by lazy { MutableLiveData("") }
    val fullname by lazy { MutableLiveData("") }
    val password by lazy { MutableLiveData("") }
    val repeatPassword by lazy { MutableLiveData("") }

    fun clickSubmit(){
        when{
            email.value.isNullOrEmpty() ->
                _showToastMessageEvent.set("Harap isi email anda")

            phonenumber.value.isNullOrEmpty() ->
                _showToastMessageEvent.set("Harap isi nomor hp anda")

            fullname.value.isNullOrEmpty() ->
                _showToastMessageEvent.set("Harap isi nama anda")

            password.value.isNullOrEmpty() ->
                _showToastMessageEvent.set("Harap isi password anda")

            password.value != repeatPassword.value ->
                _showToastMessageEvent.set("Password konfirmasi anda tidak sesuai")

            else -> proceedSubmit()
        }
    }

    private fun proceedSubmit() {
        viewModelScope.launch {
            // Register Account First
            profileRepository.register(
                email.value.orEmpty(),
                phonenumber.value.orEmpty(),
                fullname.value.orEmpty(),
                password.value.orEmpty()
            ).onFailure {
                _showToastMessageEvent.set(it.reason.message.toString())
                return@launch
            }

            // Login again to get Token
            profileRepository.signIn(phonenumber.value.orEmpty(),
                password.value.orEmpty()).onFailure {
                _showToastMessageEvent.set(it.reason.message.toString())
                return@launch
            }

            _openVerificationPageEvent.set(fullname.value.orEmpty())
        }
    }
}