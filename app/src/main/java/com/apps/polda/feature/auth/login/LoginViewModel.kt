package com.apps.polda.feature.auth.login

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.apps.polda.utils.zeecommon.ui.AppViewModel
import com.apps.polda.utils.zeecommon.util.event.LiveEvent
import com.apps.polda.utils.zeecommon.util.event.MutableLiveEvent
import com.apps.polda.utils.zeedata.repository.ProfileRepository
import com.natpryce.onFailure
import kotlinx.coroutines.launch

class LoginViewModel @ViewModelInject constructor(
    private val profileRepository: ProfileRepository
): AppViewModel() {
    private val _openRegisterPageEvent by lazy { MutableLiveEvent<Unit>() }
    val openRegisterPageEvent: LiveEvent<Unit> = _openRegisterPageEvent

    private val _loginSuccessEvent by lazy { MutableLiveEvent<Unit>() }
    val loginSuccessEvent: LiveEvent<Unit> = _loginSuccessEvent

    val phonenumber by lazy { MutableLiveData("") }
    val password by lazy { MutableLiveData("") }

    init {
        phonenumber.value = ""
        password.value = ""
    }

    fun clickSubmit(){
        when {
            phonenumber.value.isNullOrEmpty() -> _showToastMessageEvent.set("Harap isi nomor telepon anda")
            password.value.isNullOrEmpty() -> _showToastMessageEvent.set("Harap isi password anda")
            else -> proceedSubmit()
        }
    }

    private fun proceedSubmit() {
        viewModelScope.launch {
            profileRepository.signIn(
                phonenumber.value.orEmpty(),
                password.value.orEmpty()
            ).onFailure {
                val reason = it.reason.localizedMessage ?: "Connection Trouble!"
                _showToastMessageEvent.set("Error $reason")
                return@launch
            }

            _loginSuccessEvent.set(Unit)
        }
    }

    fun clickRegister(){
        _openRegisterPageEvent.set(Unit)
    }
}