package com.apps.polda.feature.chat

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.apps.polda.datasource.FirebaseChildEventListener
import com.apps.polda.datasource.model.FirebaseRealtimeModel
import com.apps.polda.utils.getCurrentDate
import com.apps.polda.utils.zeecommon.extension.add
import com.apps.polda.utils.zeecommon.extension.defaultDateFormat
import com.apps.polda.utils.zeecommon.ui.AppViewModel
import com.apps.polda.utils.zeecommon.util.event.LiveEvent
import com.apps.polda.utils.zeecommon.util.event.MutableLiveEvent
import com.apps.polda.utils.zeedata.api.response.itemresponse.ConversationResponse
import com.apps.polda.utils.zeedata.repository.ChatRepository
import com.apps.polda.utils.zeedata.repository.ProfileRepository
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.natpryce.onFailure
import kotlinx.coroutines.launch
import java.io.File

class ConversationPageViewModel @ViewModelInject constructor(
    private val mChatRepository: ChatRepository,
    private val mProfileRepository: ProfileRepository
) : AppViewModel(){

    private val _onSuccessSendEvent by lazy { MutableLiveEvent<Unit>() }
    val onSuccessSendEvent: LiveEvent<Unit> = _onSuccessSendEvent

    private val _openAttachImageEvent by lazy { MutableLiveEvent<Unit>() }
    val openAttachImageEvent: LiveEvent<Unit> = _openAttachImageEvent

    private val _scrollToEndEvent by lazy { MutableLiveEvent<Unit>() }
    val scrollToEndEvent: LiveEvent<Unit> = _scrollToEndEvent

    private val myID by lazy { MutableLiveData<String>() }
    private val username by lazy { MutableLiveData<String>() }

    val textNewChat by lazy { MutableLiveData("") }
    val image by lazy { MutableLiveData<File>() }

    private val isGroup by lazy { MutableLiveData(false) }
    val title by lazy { MutableLiveData("") }
    val listChat by lazy {
        MutableLiveData<List<ConversationResponse>>().apply {
            value = listOf()
        }
    }

    private val mFirebaseReference by lazy {
        when(isGroup.value){
            true -> {
                Firebase.database.reference
                    .child("groupnotif")
                    .child(myID.value.orEmpty())
            }
            else ->{
                Firebase.database.reference
                    .child("notif")
                    .child(myID.value.orEmpty())
            }
        }

    }

    private fun listenToRealtimeDb() {
        mFirebaseReference
            .addChildEventListener(object : FirebaseChildEventListener() {
                override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                    val chat = snapshot.getValue(FirebaseRealtimeModel::class.java)
                    chat?.let {
                        if(username.value.equals(it.from)){
                            launchConversation()
                            snapshot.ref.removeValue()
                        }
                    }
                }
            })
    }


    fun setup(ttl: String, id: String, isitGroup: Boolean) {
        title.value = ttl
        username.value = id
        isGroup.value = isitGroup
        loadID()
        listenToRealtimeDb()
        launchConversation()
    }

    fun loadID(){
        viewModelScope.launch {
            myID.value = mProfileRepository.usernameFromDB().onFailure {
                _showToastMessageEvent.set(it.reason.localizedMessage ?: "Please upgrade your apps")
                return@launch
            }
        }
    }

    fun launchConversation() {
        viewModelScope.launch {
            listChat.value =
                when(isGroup.value){
                    true -> {
                        val result = mChatRepository.getListConversationGroup(username.value.orEmpty()).onFailure {
                            return@launch
                        }
                        for (i in result){ i.isGroup = true }

                        result
                    }
                    else -> {
                        mChatRepository.getListConversation(username.value.orEmpty()).onFailure {
                            return@launch
                        }
                    }
                }

            _scrollToEndEvent.set(Unit)
        }
    }

    fun openAttachDialog() {
        _openAttachImageEvent.set(Unit)
    }

    fun send() {
        listChat.add(
            ConversationResponse(
                id = listChat.value?.size,
                sender = true,
                content = textNewChat.value.orEmpty(),
                createdAt = getCurrentDate(),
                isGroup = isGroup.value ?: false
            )
        )
        _onSuccessSendEvent.set(Unit)


        viewModelScope.launch {

            val result =
                when(isGroup.value){
                    true -> {
                        mChatRepository.postMessageGroup(
                            username.value.orEmpty(),
                            textNewChat.value.orEmpty()
                        )
                    }
                    else -> {
                        mChatRepository.postMessage(
                            username.value.orEmpty(),
                            textNewChat.value.orEmpty()
                        )
                    }
                }

            result.onFailure {
                launchConversation()
                if(!it.equals("Success")){
                    _showToastMessageEvent.set("Sorry "+ (it.reason.localizedMessage ?: "Connection in trouble!"))
                    Log.w("Error", it.reason.localizedMessage.orEmpty() +" - OR -  "+ it.toString())
                }
                return@launch
            }

            launchConversation()
        }
    }

    fun uploadImage(image: File) {
        viewModelScope.launch {
            val result = mChatRepository.postImage(
                username.value.orEmpty(),
                image,
                isGroup.value ?: false
            )

            result.onFailure {
                launchConversation()
                if(!it.equals("Success")){
                    _showToastMessageEvent.set("Sorry "+ (it.reason.localizedMessage ?: "Connection in trouble!"))
                    Log.w("Error", it.reason.localizedMessage.orEmpty() +" - OR -  "+ it.toString())
                }
                return@launch
            }

            launchConversation()
        }
    }

    fun uploadLocation(latLng: String) {
        viewModelScope.launch {

            val result =
                when(isGroup.value){
                    true -> {
                        mChatRepository.postMessageGroup(
                            dest_username = username.value.orEmpty(),
                            chat = latLng,
                            media_type = 3
                        )
                    }
                    else -> {
                        mChatRepository.postMessage(
                            dest_username = username.value.orEmpty(),
                            chat = latLng,
                            media_type = 3
                        )
                    }
                }

            result.onFailure {
                launchConversation()
                if(!it.equals("Success")){
                    _showToastMessageEvent.set("Sorry "+ (it.reason.localizedMessage ?: "Connection in trouble!"))
                    Log.w("Error", it.reason.localizedMessage.orEmpty() +" - OR -  "+ it.toString())
                }
                return@launch
            }

            launchConversation()
        }
    }
}