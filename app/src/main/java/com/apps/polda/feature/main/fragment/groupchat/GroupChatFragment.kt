package com.apps.polda.feature.main.fragment.groupchat

import android.widget.PopupMenu
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.apps.polda.R
import com.apps.polda.common.adapter.SampleAdapter
import com.apps.polda.common.adapter.SampleListener
import com.apps.polda.common.adapter.chat.ChatListAdapter
import com.apps.polda.databinding.FragmentGroupChatBinding
import com.apps.polda.databinding.FragmentPrivateChatBinding
import com.apps.polda.feature.chat.ConversationPageActivity
import com.apps.polda.feature.contact.FriendRequestActivity
import com.apps.polda.feature.main.MainViewModel
import com.apps.polda.feature.main.fragment.privatechat.PrivateChatViewModel
import com.apps.polda.feature.profile.ProfileActivity
import com.apps.polda.utils.sampleList
import com.apps.polda.utils.zeecommon.extension.startActivity
import com.apps.polda.utils.zeecommon.ui.AppFragment
import com.apps.polda.utils.zeecommon.ui.HasObservers
import com.apps.polda.utils.zeecommon.ui.HasParentViewModel
import com.apps.polda.utils.zeecommon.ui.HasViews
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class GroupChatFragment : AppFragment<FragmentGroupChatBinding, GroupChatViewModel>
    (R.layout.fragment_group_chat), HasParentViewModel, HasViews, HasObservers {

    override val parentViewModel by activityViewModels<MainViewModel>()
    override val viewModel by viewModels <GroupChatViewModel>()

    private val mChatListAdapter by lazy { ChatListAdapter(viewModel) }

    override fun onResume() {
        super.onResume()
        viewModel.fetchChatList()
    }

    override fun setupViews() {
        viewBinding.rvGroupChat.adapter = mChatListAdapter
    }

    override fun setupObservers() {
        viewModel.openMoreMenuEvent.observe(this) {
            val popupmenu = PopupMenu(requireContext(), viewBinding.btnOption)
            popupmenu.menuInflater.inflate(R.menu.option_menu, popupmenu.menu)

            popupmenu.setOnMenuItemClickListener { item ->
                when(item.itemId){
                    R.id.friend_request -> {
                        requireActivity().startActivity<FriendRequestActivity>()
                    }
                    R.id.setting ->{
                        ProfileActivity.launch(requireActivity())
                    }
                }
                true
            }
            popupmenu.show()
        }
        viewModel.openChattEvent.observe(this) {
            ConversationPageActivity.launch(requireActivity(), it.name.orEmpty(), it.destUsername.orEmpty(), true)
        }
    }
}