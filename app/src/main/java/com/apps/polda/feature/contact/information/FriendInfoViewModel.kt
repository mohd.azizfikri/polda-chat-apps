package com.apps.polda.feature.contact.information

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.apps.polda.utils.zeecommon.extension.orNol
import com.apps.polda.utils.zeecommon.ui.AppViewModel
import com.apps.polda.utils.zeecommon.util.event.LiveEvent
import com.apps.polda.utils.zeecommon.util.event.MutableLiveEvent
import com.apps.polda.utils.zeedata.repository.ContactRepository
import com.natpryce.onFailure
import kotlinx.coroutines.launch

class FriendInfoViewModel @ViewModelInject constructor(
    private val mContactRepository: ContactRepository
) : AppViewModel() {
    private val _addAsNewFriend by lazy { MutableLiveEvent<Unit>() }
    val addAsNewFriend: LiveEvent<Unit> = _addAsNewFriend

    private val _openImageActivityEvent by lazy { MutableLiveEvent<String>() }
    val openImageActivityEvent: LiveEvent<String> = _openImageActivityEvent

    val mFriendID by lazy { MutableLiveData(0) }
    val mFriendNumber by lazy { MutableLiveData("") }
    val mFriendName by lazy { MutableLiveData("") }
    val mFriendPicture by lazy { MutableLiveData("") }
    val mIsFriend by lazy { MutableLiveData(true) }

    fun setup(idFriend: String? = null){
        idFriend?.let {
            mFriendNumber.value = idFriend
            friendData()
        } ?: _showToastMessageEvent.set("Informasi profil tidak ditemukan")
    }

    fun friendData(){
        viewModelScope.launch {
            val result = mContactRepository.searchUsername(mFriendNumber.value.orEmpty()).onFailure {
                return@launch
            }.first()
            mFriendID.value = result.id
            mFriendName.value = result.name
            mFriendPicture.value = result.profile_picture
            mIsFriend.value = result.is_friend
        }
    }

    fun openImage(){
        mFriendPicture.value?.let { _openImageActivityEvent.set(it) }
    }

    fun addAsNewFriend(){
        viewModelScope.launch {
            mContactRepository.sendReqAddFriend(mFriendID.value.orNol().toString()).onFailure { error ->
                _showToastMessageEvent.set(error.reason.localizedMessage ?: "Connection trouble!")
                return@launch
            }
            _showToastMessageEvent.set("Berhasil mengirim permintaan pertemanan ke ${mFriendName.value.orEmpty()} ")
        }
    }

}