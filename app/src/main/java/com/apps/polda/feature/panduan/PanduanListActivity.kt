package com.apps.polda.feature.panduan

import androidx.activity.viewModels
import com.apps.polda.R
import com.apps.polda.common.adapter.feature.ListPanduanAdapter
import com.apps.polda.databinding.ActivityPanduanListBinding
import com.apps.polda.utils.zeecommon.ui.AppActivity
import com.apps.polda.utils.zeecommon.ui.HasObservers
import com.apps.polda.utils.zeecommon.ui.HasViews
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PanduanListActivity : AppActivity<ActivityPanduanListBinding, PanduanListViewModel>
    (R.layout.activity_panduan_list), HasViews, HasObservers {
    override val viewModel by viewModels<PanduanListViewModel>()

    private val mPanduanAdapter by lazy { ListPanduanAdapter(viewModel) }

    override fun setupViews() {
        viewBinding.ltoolbar.setOnPrimaryIconClick {
            finish()
        }
        viewBinding.ltoolbar.setOnSecondaryIconClick {
            viewModel.isSearchOpen.value = viewModel.isSearchOpen.value?.let {
                when(it) {
                    false -> {
                        viewBinding.ltoolbar.setSecondaryIconImage(R.drawable.ic_close)
                        true
                    }
                    else -> {
                        viewModel.fetchList()
                        viewBinding.ltoolbar.setSecondaryIconImage(R.drawable.ic_search_white)
                        false
                    }
                }
            }
        }

        viewBinding.rvPanduan.adapter = mPanduanAdapter
    }

    override fun setupObservers() {
        viewModel.onCLickItemPanduanEvent.observe(this) {
            PanduanDetailActivity.launch(this, Gson().toJson(it))
        }
        viewModel.changeIconToSearchEvent.observe(this) {
            viewBinding.ltoolbar.setSecondaryIconImage(R.drawable.ic_search_white)
        }
    }

}