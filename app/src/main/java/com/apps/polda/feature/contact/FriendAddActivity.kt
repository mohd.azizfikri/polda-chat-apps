package com.apps.polda.feature.contact

import androidx.activity.viewModels
import com.apps.polda.R
import com.apps.polda.common.adapter.contact.FriendAddAdapter
import com.apps.polda.databinding.ActivityFriendAddBinding
import com.apps.polda.feature.profile.ProfileActivity
import com.apps.polda.utils.zeecommon.ui.AppActivity
import com.apps.polda.utils.zeecommon.ui.HasObservers
import com.apps.polda.utils.zeecommon.ui.HasViews
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FriendAddActivity : AppActivity<ActivityFriendAddBinding, FriendAddViewModel>
    (R.layout.activity_friend_add), HasViews, HasObservers {

    override val viewModel by viewModels<FriendAddViewModel>()

    private val mFriendAddAdapter by lazy { FriendAddAdapter(viewModel) }

    override fun setupViews() {
        viewBinding.backButton.setOnClickListener {
            finish()
        }

        viewBinding.rvContactlist.adapter = mFriendAddAdapter
    }

    override fun setupObservers() {
        viewModel.openDetailProfileEvent.observe(this) {
            ProfileActivity.launch(this, Gson().toJson(it))
        }
        viewModel.addAsFriendSuccessEvent.observe(this) {
            finish()
        }
    }
}