package com.apps.polda.feature.main

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.apps.polda.R
import com.apps.polda.utils.zeecommon.ui.AppViewModel
import com.apps.polda.utils.zeecommon.util.event.LiveEvent
import com.apps.polda.utils.zeecommon.util.event.MutableLiveEvent
import com.apps.polda.utils.zeedata.repository.ProfileRepository
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.natpryce.onFailure
import kotlinx.coroutines.launch

class MainViewModel @ViewModelInject constructor(
    private val mUserRepository: ProfileRepository
) : AppViewModel() {

    private val _openIntroPageEvent by lazy { MutableLiveEvent<Unit>() }
    val openIntroPageEvent: LiveEvent<Unit> = _openIntroPageEvent

    private val _openOtpPageEvent by lazy { MutableLiveEvent<Unit>() }
    val openOtpPageEvent: LiveEvent<Unit> = _openOtpPageEvent

    fun registerFCM() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("MainViewModel", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result

            // Log and toast
            Log.d("MainViewModel", token)

            // Send to server
            sendTokenToServer(token)
        })
    }

    fun sendTokenToServer(token: String){
        /*mUserRepository.isSignedIn().onFailure {
           return
       }*/

       /*FirebaseMessaging
           .getInstance()
           .token
           .addOnCompleteListener {
               if (!it.isSuccessful) {
                   return@addOnCompleteListener
               }
               viewModelScope.launch {
                   mUserRepository
                       .updateFCM(it.result)
                       .onFailure { return@launch }
               }
           }*/
        viewModelScope.launch {
            mUserRepository.updateFCM(token).onFailure { return@launch }
        }
    }

    fun openOrSignIn(block: () -> Unit): Boolean {
        block()
        return true // Sample doang abis jadi hapus yg asli dibawah
       /* mUserRepository.isNotSignedIn().onFailure {
            mUserRepository.isOnVerificationProcess().onFailure {
                block()
                return true
            }
            _openOtpPageEvent.set(Unit)
            return false
        }
        _openIntroPageEvent.set(Unit)
        return false*/
    }

}