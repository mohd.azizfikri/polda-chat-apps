package com.apps.polda.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.components.ServiceComponent
import com.apps.polda.utils.zeedata.repository.*

@Module
@InstallIn(ActivityComponent::class, ServiceComponent::class)
object RepositoryModule {

    @Provides
    fun provideProfileRepository() = ProfileRepository()

    @Provides
    fun provideCommonRepository() = CommonRepository()

    @Provides
    fun provideChatRepository() = ChatRepository()

    @Provides
    fun provideContactRepository() = ContactRepository()

}