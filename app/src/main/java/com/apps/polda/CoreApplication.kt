package com.apps.polda

import android.app.Application
import com.apps.polda.utils.Util
import com.apps.polda.utils.zeecommon.CommonProvider
import com.apps.polda.utils.zeedata.DataConfiguration
import com.apps.polda.utils.zeedata.DataProvider
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CoreApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        Util.init(this)
        CommonProvider.init(BR.viewmodel)
        DataProvider.init(
            this, DataConfiguration(
                remoteHost = "https://poldachatssystems.site/",
                remoteTimeout = 60L,
                debug = BuildConfig.DEBUG
            )
        )
    }
}