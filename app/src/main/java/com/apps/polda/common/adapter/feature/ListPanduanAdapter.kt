package com.apps.polda.common.adapter.feature

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.apps.polda.databinding.ItemPanduanListBinding
import com.apps.polda.utils.zeecommon.SelectableData
import com.apps.polda.utils.zeecommon.util.list.AppRecyclerView
import com.apps.polda.utils.zeedata.api.response.itemresponse.PanduanResponse

class ListPanduanAdapter (
    private val mListener: PanduanItemListener
) : AppRecyclerView<ItemPanduanListBinding, SelectableData<PanduanResponse>>(DIFF_UTIL) {

    override fun onCreateViewBinding(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): ItemPanduanListBinding = ItemPanduanListBinding.inflate(inflater, parent, false)

    override fun onPrepareBindViewHolder(
        binding: ItemPanduanListBinding,
        model: SelectableData<PanduanResponse>
    ) {
        binding.data = model.origin

        binding.llLayout.setOnClickListener {
            mListener.onClickPanduanItem(model.origin)
        }
    }

    interface PanduanItemListener {
        fun onClickPanduanItem(data: PanduanResponse)
    }

    companion object {

        val DIFF_UTIL = object : DiffUtil.ItemCallback<SelectableData<PanduanResponse>>() {

            override fun areItemsTheSame(
                oldItem: SelectableData<PanduanResponse>,
                newItem: SelectableData<PanduanResponse>
            ): Boolean {
                return oldItem.origin == newItem.origin
            }

            override fun areContentsTheSame(
                oldItem: SelectableData<PanduanResponse>,
                newItem: SelectableData<PanduanResponse>
            ): Boolean {
                return oldItem.origin.id == newItem.origin.id
            }

        }
    }

}