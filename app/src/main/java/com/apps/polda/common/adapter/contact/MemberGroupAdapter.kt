package com.apps.polda.common.adapter.contact

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.apps.polda.databinding.ItemGroupMemberBinding
import com.apps.polda.utils.zeecommon.SelectableData
import com.apps.polda.utils.zeecommon.util.list.AppRecyclerView
import com.apps.polda.utils.zeedata.api.response.itemresponse.utils.MemberInfo

class MemberGroupAdapter (private val mListener: ListMemberGroupListener
    ) : AppRecyclerView<ItemGroupMemberBinding, SelectableData<MemberInfo>>(DIFF_UTIL) {

        override fun onCreateViewBinding(
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ItemGroupMemberBinding = ItemGroupMemberBinding.inflate(inflater, parent, false)

        override fun onPrepareBindViewHolder(
            binding: ItemGroupMemberBinding,
            model: SelectableData<MemberInfo>
        ) {
            binding.data = model.origin

            binding.llLayout.setOnClickListener {
                mListener.onClickMemberGroup(model.origin)
            }
        }

        interface ListMemberGroupListener {
            fun onClickMemberGroup(data: MemberInfo)
        }

        companion object {

            val DIFF_UTIL = object : DiffUtil.ItemCallback<SelectableData<MemberInfo>>() {

                override fun areItemsTheSame(
                    oldItem: SelectableData<MemberInfo>,
                    newItem: SelectableData<MemberInfo>
                ): Boolean {
                    return oldItem.origin == newItem.origin
                }

                override fun areContentsTheSame(
                    oldItem: SelectableData<MemberInfo>,
                    newItem: SelectableData<MemberInfo>
                ): Boolean {
                    return oldItem.origin == newItem.origin
                }

            }
        }
    }