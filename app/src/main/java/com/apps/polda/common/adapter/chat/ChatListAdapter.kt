package com.apps.polda.common.adapter.chat

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.apps.polda.databinding.ItemListChatBinding
import com.apps.polda.utils.zeecommon.SelectableData
import com.apps.polda.utils.zeecommon.util.list.AppRecyclerView
import com.apps.polda.utils.zeedata.api.response.itemresponse.ChatResponse

class ChatListAdapter (
    private val mListener: ChatItemListener
) : AppRecyclerView<ItemListChatBinding, ChatResponse>(DIFF_UTIL) {

    override fun onCreateViewBinding(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): ItemListChatBinding = ItemListChatBinding.inflate(inflater, parent, false)

    override fun onPrepareBindViewHolder(
        binding: ItemListChatBinding,
        model: ChatResponse
    ) {
        binding.data = model

        binding.llLayout.setOnClickListener {
            mListener.onClickChatItem(model)
        }

        binding.llLayout.setOnLongClickListener {
            mListener.onHoldChatItem(model)
            true
        }

        binding.imgBtnDelete.setOnClickListener {
            mListener.onClickDeleteChatItem(model)
        }

        binding.imgBtnCancel.setOnClickListener {
            mListener.onClickCancelSelectedItem(model)
        }
    }

    interface ChatItemListener {
        fun onClickChatItem(data: ChatResponse)
        fun onHoldChatItem(data: ChatResponse)
        fun onClickDeleteChatItem(data: ChatResponse)
        fun onClickCancelSelectedItem(data: ChatResponse)
    }

    companion object {

        val DIFF_UTIL = object : DiffUtil.ItemCallback<ChatResponse>() {

            override fun areItemsTheSame(
                oldItem: ChatResponse,
                newItem: ChatResponse
            ): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(
                oldItem: ChatResponse,
                newItem: ChatResponse
            ): Boolean {
                return oldItem.chatSession == newItem.chatSession
            }

        }
    }

}