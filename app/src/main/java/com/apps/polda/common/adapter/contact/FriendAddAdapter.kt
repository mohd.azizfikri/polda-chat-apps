package com.apps.polda.common.adapter.contact

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.apps.polda.databinding.ItemFriendAddBinding
import com.apps.polda.utils.zeecommon.SelectableData
import com.apps.polda.utils.zeecommon.util.list.AppRecyclerView
import com.apps.polda.utils.zeedata.api.response.itemresponse.FriendResponse

class FriendAddAdapter(
    private val mListener: ListFriendItemListener
) : AppRecyclerView<ItemFriendAddBinding, SelectableData<FriendResponse>>(DIFF_UTIL) {

    override fun onCreateViewBinding(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): ItemFriendAddBinding = ItemFriendAddBinding.inflate(inflater, parent, false)

    override fun onPrepareBindViewHolder(
        binding: ItemFriendAddBinding,
        model: SelectableData<FriendResponse>
    ) {
        binding.data = model.origin

        binding.imageView.setOnClickListener {
            mListener.onClickListFriendItem(model.origin)
        }

        binding.idText.setOnClickListener {
            mListener.onClickListFriendItem(model.origin)
        }

        binding.addAsFriend.setOnClickListener {
            mListener.onClickAddAsFriend(model.origin)
        }
    }

    interface ListFriendItemListener {
        fun onClickListFriendItem(data: FriendResponse)
        fun onClickAddAsFriend(data: FriendResponse)
    }

    companion object {

        val DIFF_UTIL = object : DiffUtil.ItemCallback<SelectableData<FriendResponse>>() {

            override fun areItemsTheSame(
                oldItem: SelectableData<FriendResponse>,
                newItem: SelectableData<FriendResponse>
            ): Boolean {
                return oldItem.origin == newItem.origin
            }

            override fun areContentsTheSame(
                oldItem: SelectableData<FriendResponse>,
                newItem: SelectableData<FriendResponse>
            ): Boolean {
                return oldItem.origin/*.id*/ == newItem.origin/*.id*/
            }

        }
    }
}