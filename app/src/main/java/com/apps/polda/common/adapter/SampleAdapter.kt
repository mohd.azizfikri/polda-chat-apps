package com.apps.polda.common.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.apps.polda.R

class SampleAdapter (
    private val items : ArrayList<String>,
    val layout : Int,
    val listener: SampleListener
) : RecyclerView.Adapter<SampleViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SampleViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return SampleViewHolder(inflater, parent, layout)
    }

    override fun onBindViewHolder(holder: SampleViewHolder, position: Int) {
        val item : String = items[position]

        holder.bind(item)

        holder.itemView.setOnClickListener {
            listener.onSampleClick(item)
        }
    }

    override fun getItemCount(): Int = items.size

}

class SampleViewHolder (inflater: LayoutInflater, parent: ViewGroup, layout: Int) :
    RecyclerView.ViewHolder(inflater.inflate(layout, parent, false)) {

    fun bind(item : String) {
        try{
            (itemView.findViewById<TextView>(R.id.id_text)).text = "Sample ke- " + item
        }catch(e: Exception){
            Log.e("Error", e.localizedMessage)
        }
    }
}

abstract class SampleListener(){
    abstract fun onSampleClick(item: String)
}