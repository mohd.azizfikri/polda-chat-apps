package com.apps.polda.common.adapter.chat

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import coil.load
import com.apps.polda.databinding.ItemListChatBubbleBinding
import com.apps.polda.feature.contact.information.LargeImageActivity
import com.apps.polda.utils.zeecommon.util.list.AppRecyclerView
import com.apps.polda.utils.zeedata.api.response.itemresponse.ConversationResponse


class ChatBubbleListAdapter : AppRecyclerView<ItemListChatBubbleBinding, ConversationResponse>(DIFF_UTIL) {

    override fun onCreateViewBinding(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): ItemListChatBubbleBinding = ItemListChatBubbleBinding.inflate(inflater, parent, false)

    override fun onPrepareBindViewHolder(binding: ItemListChatBubbleBinding, model: ConversationResponse) {
        binding.data = model

        val cbbl = if (model.sender) binding.cbblOut else binding.cbblIn

        cbbl.setMessage(model.content)
        cbbl.setTime(model.createdAt)

        if(model.type == 4){
            cbbl.setLocation(true, model.content)
        }else{
            cbbl.setLocation(false)
        }

        if (!model.file.isNullOrEmpty()) {
            cbbl.setImage {
                it?.load(model.file)
                it?.visibility = View.VISIBLE
                it?.setOnClickListener {
                    LargeImageActivity.launch((it.context as Activity), LargeImageActivity.EXTRA_FILE_TYPE_URL, model.file)
                }
            }
        } else {
            cbbl.setImage { it?.visibility = View.GONE }
        }

        if(model.isGroup && !model.sender){
            cbbl.setSender {
                it?.text = model.name
                it?.visibility = View.VISIBLE
            }
        }else{
            cbbl.setSender { it?.visibility = View.GONE }
        }
    }

    companion object {
        val DIFF_UTIL = object : DiffUtil.ItemCallback<ConversationResponse>() {
            override fun areItemsTheSame(oldItem: ConversationResponse, newItem: ConversationResponse): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: ConversationResponse, newItem: ConversationResponse): Boolean {
                return oldItem.id == newItem.id
            }

        }
    }

}