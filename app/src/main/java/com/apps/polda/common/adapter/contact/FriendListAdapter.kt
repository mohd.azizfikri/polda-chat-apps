package com.apps.polda.common.adapter.contact

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.apps.polda.databinding.ItemFriendListBinding
import com.apps.polda.utils.zeecommon.SelectableData
import com.apps.polda.utils.zeecommon.util.list.AppRecyclerView
import com.apps.polda.utils.zeedata.api.response.itemresponse.FriendResponse

class FriendListAdapter(
    private val mListener: ListFriendItemListener
) : AppRecyclerView<ItemFriendListBinding, SelectableData<FriendResponse>>(DIFF_UTIL) {

    override fun onCreateViewBinding(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): ItemFriendListBinding = ItemFriendListBinding.inflate(inflater, parent, false)

    override fun onPrepareBindViewHolder(
        binding: ItemFriendListBinding,
        model: SelectableData<FriendResponse>
    ) {
        binding.data = model.origin

        binding.llLayout.setOnClickListener {
            mListener.onClickListFriendItem(model.origin)
        }
    }

    interface ListFriendItemListener {
        fun onClickListFriendItem(data: FriendResponse)
    }

    companion object {

        val DIFF_UTIL = object : DiffUtil.ItemCallback<SelectableData<FriendResponse>>() {

            override fun areItemsTheSame(
                oldItem: SelectableData<FriendResponse>,
                newItem: SelectableData<FriendResponse>
            ): Boolean {
                return oldItem.origin == newItem.origin
            }

            override fun areContentsTheSame(
                oldItem: SelectableData<FriendResponse>,
                newItem: SelectableData<FriendResponse>
            ): Boolean {
                return oldItem.origin/*.id*/ == newItem.origin/*.id*/
            }

        }
    }
}