package com.apps.polda.common.adapter.feature

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.apps.polda.databinding.ItemVideoListBinding
import com.apps.polda.utils.zeecommon.SelectableData
import com.apps.polda.utils.zeecommon.util.list.AppRecyclerView
import com.apps.polda.utils.zeedata.api.response.itemresponse.VideoResponse

class ListVideoAdapter(
    private val mListener: VideoItemListener
) : AppRecyclerView<ItemVideoListBinding, SelectableData<VideoResponse>>(DIFF_UTIL) {

    override fun onCreateViewBinding(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): ItemVideoListBinding = ItemVideoListBinding.inflate(inflater, parent, false)

    override fun onPrepareBindViewHolder(
        binding: ItemVideoListBinding,
        model: SelectableData<VideoResponse>
    ) {
        binding.data = model.origin

        binding.llLayout.setOnClickListener {
            mListener.onClickVideoItem(model.origin)
        }
    }

    interface VideoItemListener {
        fun onClickVideoItem(data: VideoResponse)
    }

    companion object {

        val DIFF_UTIL = object : DiffUtil.ItemCallback<SelectableData<VideoResponse>>() {

            override fun areItemsTheSame(
                oldItem: SelectableData<VideoResponse>,
                newItem: SelectableData<VideoResponse>
            ): Boolean {
                return oldItem.origin == newItem.origin
            }

            override fun areContentsTheSame(
                oldItem: SelectableData<VideoResponse>,
                newItem: SelectableData<VideoResponse>
            ): Boolean {
                return oldItem.origin.id == newItem.origin.id
            }

        }
    }

}
