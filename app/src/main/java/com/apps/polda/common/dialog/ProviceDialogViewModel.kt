package com.apps.polda.common.dialog

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.apps.polda.common.adapter.ProvinceAdapter
import com.apps.polda.datasource.model.Item
import com.apps.polda.utils.zeecommon.ui.AppViewModel
import com.apps.polda.utils.zeecommon.util.event.LiveEvent
import com.apps.polda.utils.zeecommon.util.event.MutableLiveEvent
import com.apps.polda.utils.zeecommon.util.list.AppDataWrapperWithMapper
import com.apps.polda.utils.zeedata.repository.CommonRepository
import kotlinx.coroutines.launch

class ProvinceDialogViewModel @ViewModelInject constructor(
    private val commonRepository: CommonRepository
) : AppViewModel(), ProvinceAdapter.itemAdapterListener {

    private val _onResultShouldPassedEvent by lazy { MutableLiveEvent<Item>() }
    val onResultShouldPassedEvent: LiveEvent<Item> = _onResultShouldPassedEvent

    val provinceWrapper by lazy { AppDataWrapperWithMapper(Item::mapFromProvince) }

    fun setup(){
        fetchData()
    }

    private fun fetchData() {
        viewModelScope.launch {
            provinceWrapper.load {
                commonRepository.getProvince()
            }
        }
    }

    override fun onClickItem(data: Item) {
        _onResultShouldPassedEvent.set(data)
    }

}
