package com.apps.polda.common.adapter.contact

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.apps.polda.databinding.ItemFriendRequestBinding
import com.apps.polda.utils.zeecommon.SelectableData
import com.apps.polda.utils.zeecommon.util.list.AppRecyclerView
import com.apps.polda.utils.zeedata.api.response.itemresponse.FriendResponse

class FriendRequestAdapter  (
    private val mListener: FriendRequestItemListener
) : AppRecyclerView<ItemFriendRequestBinding, SelectableData<FriendResponse>>(DIFF_UTIL) {

    override fun onCreateViewBinding(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): ItemFriendRequestBinding = ItemFriendRequestBinding.inflate(inflater, parent, false)

    override fun onPrepareBindViewHolder(
        binding: ItemFriendRequestBinding,
        model: SelectableData<FriendResponse>
    ) {
        binding.data = model.origin

        binding.imageView.setOnClickListener {
            mListener.onClickFriendRequestItem(model.origin)
        }

        binding.idText.setOnClickListener {
            mListener.onClickFriendRequestItem(model.origin)
        }

        binding.btnAccept.setOnClickListener {
            mListener.onActionAcceptFriendRequest(model.origin)
        }

        binding.btnReject.setOnClickListener {
            mListener.onActionRejectFriendRequest(model.origin)
        }
    }

    interface FriendRequestItemListener {
        fun onClickFriendRequestItem(data: FriendResponse)
        fun onActionAcceptFriendRequest(data: FriendResponse)
        fun onActionRejectFriendRequest(data: FriendResponse)
    }

    companion object {

        val DIFF_UTIL = object : DiffUtil.ItemCallback<SelectableData<FriendResponse>>() {

            override fun areItemsTheSame(
                oldItem: SelectableData<FriendResponse>,
                newItem: SelectableData<FriendResponse>
            ): Boolean {
                return oldItem.origin == newItem.origin
            }

            override fun areContentsTheSame(
                oldItem: SelectableData<FriendResponse>,
                newItem: SelectableData<FriendResponse>
            ): Boolean {
                return oldItem.origin/*.id*/ == newItem.origin/*.id*/
            }

        }
    }
}