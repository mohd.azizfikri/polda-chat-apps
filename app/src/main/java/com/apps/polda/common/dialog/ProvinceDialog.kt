package com.apps.polda.common.dialog

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.apps.polda.R
import com.apps.polda.common.adapter.ProvinceAdapter
import com.apps.polda.utils.zeecommon.ui.AppDialogFragment
import com.apps.polda.utils.zeecommon.ui.HasObservers
import com.apps.polda.utils.zeecommon.ui.HasViews
import com.apps.polda.databinding.DialogProvinceBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProvinceDialog(
    private val mOnResultBlock: (data: String) -> Unit
) : AppDialogFragment<DialogProvinceBinding, ProvinceDialogViewModel>(R.layout.dialog_province),
    HasObservers, HasViews {

    override val viewModel by viewModels<ProvinceDialogViewModel>()
    override val dialogWindowFullscreen: Boolean = true
    private val mItemMapperAdapter by lazy { ProvinceAdapter(viewModel) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.setup()
    }

    override fun setupObservers() {
        viewModel.onResultShouldPassedEvent.observe(viewLifecycleOwner) {
            dismiss()
            mOnResultBlock(it.name)
        }
    }

    override fun setupViews() {
        viewBinding.rvProvince.adapter = mItemMapperAdapter
    }

}
