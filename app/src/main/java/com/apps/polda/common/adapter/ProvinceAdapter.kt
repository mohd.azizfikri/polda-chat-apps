package com.apps.polda.common.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.apps.polda.databinding.ItemTextOnlyBinding
import com.apps.polda.datasource.model.Item
import com.apps.polda.utils.zeecommon.util.list.AppRecyclerView

class ProvinceAdapter(
    private val mListener: itemAdapterListener
) : AppRecyclerView<ItemTextOnlyBinding, Item>(Item.DIFF_UTIL) {

    override fun onCreateViewBinding(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): ItemTextOnlyBinding = ItemTextOnlyBinding.inflate(inflater, parent, false)

    override fun onPrepareBindViewHolder(
        binding: ItemTextOnlyBinding,
        model: Item
    ) {
        binding.data = model

        binding.llItem.setOnClickListener {
            mListener.onClickItem(model)
        }
    }

    interface itemAdapterListener {
        fun onClickItem(data: Item)
    }

}
