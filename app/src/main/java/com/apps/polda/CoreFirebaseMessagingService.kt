package com.apps.polda

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.text.HtmlCompat
import com.apps.polda.feature.splash.SplashScreenActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import java.util.*
import kotlin.coroutines.CoroutineContext

@AndroidEntryPoint
open class CoreFirebaseMessagingService : FirebaseMessagingService(), CoroutineScope {

    override val coroutineContext: CoroutineContext = Dispatchers.IO

    private lateinit var notificationManager: NotificationManager
    private var channelId = "FCM_CHANNEL_ID_MTA"
    private var channelName = "FCM_CHANNEL_NAME_MTA"
    private lateinit var intent: Intent

//    @Inject
//    lateinit var userRepository: UserRepository

    override fun onNewToken(token: String) {
        Log.w("Token FBase",token)
//        launch {
//            userRepository.updateFcmToken(token).onFailure {
//                return@launch
//            }
//        }
    }

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)

        // Broadcast
        val myIntent = Intent("com.apps.polda.broadcast.refreshconversation")
        sendBroadcast(myIntent)

        Log.e("BEJIGUMMMMMMMMMMM",p0.notification.toString())
        Log.e("BEJIGUTTTTTTTTTTT",p0.data.toString())

        val notificationId = Random().nextInt(60000)

        notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val title = p0.notification?.title ?: p0.data["title"] ?: ""
        val body = p0.notification?.body ?: p0.data["message"] ?: ""
        val tipe = p0.data["tipe"] ?: ""
        val content = p0.data["content"] ?: ""

        intent = Intent(this, SplashScreenActivity::class.java).apply {
            putExtra("tipe", tipe)
            putExtra("content", content)
        }

        val pendingIntent = PendingIntent.getActivity(
            this, 0, intent, PendingIntent.FLAG_ONE_SHOT
        )

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder =
            NotificationCompat.Builder(this@CoreFirebaseMessagingService, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(HtmlCompat.fromHtml(title, HtmlCompat.FROM_HTML_MODE_COMPACT))
                .setContentText(HtmlCompat.fromHtml(body, HtmlCompat.FROM_HTML_MODE_COMPACT))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(defaultSoundUri, notificationBuilder)
        }

        notificationBuilder
            .setStyle(NotificationCompat.BigTextStyle().bigText(body))

        notificationManager.notify(notificationId, notificationBuilder.build())
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(soundUri: Uri, builder: NotificationCompat.Builder) {
        val importance = NotificationManager.IMPORTANCE_HIGH
        val channel = NotificationChannel(channelId, channelName, importance)
        channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        val audioAttributes: AudioAttributes = AudioAttributes.Builder()
            .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
            .build()
        channel.setSound(soundUri, audioAttributes)
        notificationManager.createNotificationChannel(channel)
        builder.setChannelId(channelId)
    }

}